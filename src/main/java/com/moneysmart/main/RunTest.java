package com.moneysmart.main;

import java.util.logging.Level;

import java.util.logging.Logger;

/**
 * 
 * @author Shenll Technologies Solutions
 *
 */
public class RunTest {

	/*
	 * This is the Initial Class which initiates the Project to Read Excel and
	 * store the Input values in Java Collection
	 */
	private static final Logger LOGGER = Logger.getLogger(RunTest.class.getName());

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		/* Excel config begins */
		String excelConfigPath = "D:/Testcase_Config_Genric-vicky.xls";
		String excelSheetName = "Travel";

		String startReadingExcelProductValidationData = "Product Validation";
		String stoptReadingExcelProductValidationData = "Product Validation End";

		String startReadingExcelFiltersData = "Filters";
		String stoptReadingExcelFiltersData = "Filters End";

		String startReadingExcelIfCalculationData = "Calculation Data";
		String stoptReadingExcelIfCalculationData = "Calculation Data End";

		try {

			ReadExcelInput excelInput = new ReadExcelInput();
			excelInput.readXLSFile(excelConfigPath, excelSheetName, startReadingExcelIfCalculationData,
					stoptReadingExcelIfCalculationData, startReadingExcelFiltersData, stoptReadingExcelFiltersData,
					startReadingExcelProductValidationData, stoptReadingExcelProductValidationData);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "EXCEPTION", e);
		}
	}
}

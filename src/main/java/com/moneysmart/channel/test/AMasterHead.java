package com.moneysmart.channel.test;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.model.Constants.ExcelMethodInput;
import com.model.ExcelInputData;
import com.model.ExcelTestimonialData;
import com.model.GetExcelInput;
import com.model.Constants.TestResult;
import com.selenium.base.BaseClass;

import java.util.logging.Level;
import java.util.logging.Logger;
public class AMasterHead extends BaseClass {
	
    private static final Logger LOGGER = Logger.getLogger(AMasterHead.class.getName());
    // * Title Validation
    private String exceptionString = "EXCEPTION";
    SoftAssert s_assert;
    String hyphenSymbol = "";
    @Test
    public void ATitleValidation(ITestContext testContext) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        LOGGER.info("Start the test method");
        String testMethodNameTestNG = "ATitleValidation";
        // * Excel Keys
        String MethodNameKey = "Title Validation";
        String Web_Element = "Web Element";
        // * Custom messages
        String SuccessMessage = "Title Present and validated";
        String SuccessComments = "Title in web and Title in excel are equal";
        String FailureMessage = "Title Validation Failed";
        String FailureComments = "Title in web and Title in excel are not equal";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.TitleValidation_MethodNameKey, Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.TitleValidation_MethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String masterHeadTitleText = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.TitleValidation_MethodNameKey, ExcelMethodInput.TitleValidation_TextKey);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.TitleValidation_MethodNameKey, ExcelMethodInput.Test_Description_Key);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.TitleValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.TitleValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.TitleValidation_MethodNameKey, ExcelMethodInput.Actual_Result_Key);
        String channelCountry = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Openwebpage_MethodNameKey, ExcelMethodInput.Openwebpage_Country);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            super.setUp();
            driver.get(pageUrl);
            driver.navigate().refresh();
            driver.manage().window().maximize();
            Thread.sleep(8000);
            /**
            * Set Driver commonly
            */
            ExcelInputData excelInput = ExcelInputData.getInstance();
            excelInput.setWebDriver(driver);
            if (channelCountry.contains(("id"))) {
                String TitleContent = driver.findElement(By.className(WebElement)).getText();
                LOGGER.info("Title In Web: " + TitleContent);
                Thread.sleep(5000);
                if (!TitleContent.equals(masterHeadTitleText)) {
                    resultMap.put(TestResult.R_IS_SUCCESS, false);
                    resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                    resultMap.put(TestResult.R_COMMENTS, FailureComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                   
                    } else {
                    resultMap.put(TestResult.R_IS_SUCCESS, true);
                    resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                    resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                }
                // * Common Parameter
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_DESRIPTION, description);
                resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
                String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
                if (!isMethodSuccess) {
                    if (!executeYesOrNo.equalsIgnoreCase("Y")) {
                        Assert.fail(resultMessage);
                    }
                }
                testContext.setAttribute(testMethodNameTestNG, resultMap);
                               } else {
                String masterHead = "";
                ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
                Object YAMLObjectValue = excelInputDataObject.getYAMLData();
                Map objectMap = (Map) YAMLObjectValue;
                /*
                * Master Head Details
                */
                Object masterHeadObject = objectMap.get("masthead");
                Map masterHeadMap = (Map) masterHeadObject;
                LOGGER.info("Master Head Title = " + masterHeadMap.get("title"));
                LOGGER.info("n Master Head TExt = " + masterHeadMap.get("text"));
                masterHead = masterHeadMap.get("title").toString();
                LOGGER.info("n Master Head before = " + masterHead);
                s_assert = new SoftAssert();
                LOGGER.info("executeYesOrNo TitleValidation= " + executeYesOrNo);
                if (!executeYesOrNo.equalsIgnoreCase("Y")) {
                    throw new SkipException("This method skipped");
                }
                String TitleContent = driver.findElement(By.className(WebElement)).getText();
                LOGGER.info("Title In Web: " + TitleContent);
                Thread.sleep(5000);
                if (masterHead.contains("%bankName %channelName")) {
                    masterHead = masterHead.replace("%bankName %channelName", channelName);
                }
                if (masterHead.contains("%currentYear")) {
                    masterHead = masterHead.replace("%currentYear", "2016");
                }
                LOGGER.info("n Master Head after = " + masterHead);
                if (!TitleContent.equals(masterHead)) {
                    resultMap.put(TestResult.R_IS_SUCCESS, false);
                    resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                    resultMap.put(TestResult.R_COMMENTS, FailureComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                    } else {
                    resultMap.put(TestResult.R_IS_SUCCESS, true);
                    resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                    resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                }
                // * Common Parameter
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_DESRIPTION, description);
                resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
                String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
                if (!isMethodSuccess) {
                    if (!executeYesOrNo.equalsIgnoreCase("Y")) {
                        Assert.fail(resultMessage);
                    }
                }
                testContext.setAttribute(testMethodNameTestNG, resultMap);
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
    }
    @Test
    public void AZDescriptionValidation(ITestContext testContext) {
        String testMethodNameTestNG = "AZDescriptionValidation";
      
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Description Validation";
        String Web_Element = "Web Element";
        // * Custom messages
        String SuccessMessage = "Description Present and validated";
        String SuccessComments = "Description in web and Description in excel are equal";
        String FailureMessage = "Description Validation Failed";
        String FailureComments = "Description in web and Title in excel are not equal";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.DescriptionValidation_MethodNameKey, Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.DescriptionValidation_MethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.DescriptionValidation_MethodNameKey, ExcelMethodInput.Test_Description_Key);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.DescriptionValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.DescriptionValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.DescriptionValidation_MethodNameKey, ExcelMethodInput.Actual_Result_Key);
        String channelCountry = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Openwebpage_MethodNameKey, ExcelMethodInput.Openwebpage_Country);
        String masterHeadDescriptionText = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.DescriptionValidation_MethodNameKey,
        ExcelMethodInput.Description_Validation_DescriptionKey);
        String masterHeadDetail = "";
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            if (channelCountry.contains(("id"))) {
                Thread.sleep(8000);
                String DescriptionContent = driver.findElement(By.xpath(WebElement)).getText();
                Thread.sleep(2000);
                if (!DescriptionContent.equals(masterHeadDescriptionText)) {
                    resultMap.put(TestResult.R_IS_SUCCESS, false);
                    resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                    resultMap.put(TestResult.R_COMMENTS, FailureComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                    } else {
                    resultMap.put(TestResult.R_IS_SUCCESS, true);
                    resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                    resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                }
                // * Common Parameter
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_DESRIPTION, description);
                resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
                String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
                if (!isMethodSuccess) {
                    if (!executeYesOrNo.equalsIgnoreCase("Y")) {
                        Assert.fail(resultMessage);
                    }
                }
                testContext.setAttribute(testMethodNameTestNG, resultMap);
                } else {
                ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
                Object YAMLObjectValue = excelInputDataObject.getYAMLData();
                Map objectMap = (Map) YAMLObjectValue;
                // Master Head Details
                Object masterHeadObject = objectMap.get("masthead");
                Map masterHeadMap = (Map) masterHeadObject;
                LOGGER.info("Master Head Title = " + masterHeadMap.get("title"));
                LOGGER.info("n Master Head Text = " + masterHeadMap.get("text"));
                masterHeadDetail = masterHeadMap.get("text").toString();
                s_assert = new SoftAssert();
                LOGGER.info("executeYesOrNo Description Validation= " + executeYesOrNo);
                String currentWebUrl = driver.getCurrentUrl();
                Thread.sleep(9000);
                String DescriptionContent = driver.findElement(By.xpath(WebElement)).getText();
                Thread.sleep(8000);
                if (masterHeadDetail.contains("%bankName ")) {
                    masterHeadDetail = masterHeadDetail.replace("%bankName ", "");
                }
                LOGGER.info("Desc In Web: " + DescriptionContent);
                if (!DescriptionContent.equals(masterHeadDetail)) {
                    resultMap.put(TestResult.R_IS_SUCCESS, false);
                    resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                    resultMap.put(TestResult.R_COMMENTS, FailureComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                    Assert.fail(FailureMessage);
                    } else {
                    resultMap.put(TestResult.R_IS_SUCCESS, true);
                    resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                    resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                }
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_DESRIPTION, description);
                resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
                resultMap.put(ExcelMethodInput.Openwebpage_ChaneelNameKey, channelName);
                resultMap.put(ExcelMethodInput.Openwebpage_ChanellURlKey, pageUrl);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
                String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
                testContext.setAttribute(testMethodNameTestNG, resultMap);
                if (!isMethodSuccess) {
                    if (executeYesOrNo.equalsIgnoreCase("Y")) {
                        Assert.fail(resultMessage);
                    }
                }
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
    }
    @Test
    public void Bankwidget(ITestContext testContext) {
        String testMethodNameTestNG = "Bankwidget";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Bank widget";
        String Web_Element = "Web Element";
        String SuccessMessage = "Bank Widget Present in the Banner";
        String SuccessComments = "Bank Widget Present";
        String FailureMessage = "Bank Widget Validation Failed";
        String FailureComments = "Bank Widget Not Present in the Banner";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BankWidgetValidation_MethodNameKey, Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BankWidgetValidation_MethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BankWidgetValidation_MethodNameKey, ExcelMethodInput.Test_Description_Key);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BankWidgetValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BankWidgetValidation_MethodNameKey, ExcelMethodInput.Actual_Result_Key);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BannerValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
       
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Bank widget Validation= " + executeYesOrNo);
            Thread.sleep(10000);
            WebElement elements = driver.findElement(By.id(WebElement));
            Thread.sleep(5000);
            // Checking Area
            if (elements.isDisplayed()) {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            }
            // * Common Parameter
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(ExcelMethodInput.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(ExcelMethodInput.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
    }
    @Test
    public void BannerValidation(ITestContext testContext) {
        String testMethodNameTestNG = "BannerValidation";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Banner";
        String BannerurlKey = "Image Url";
        String Web_Element = "Web Element";
        // * Custom messagess
        String SuccessMessage = "Banner Present in the Website and Validated";
        String SuccessComments = "Banner in web and Banner in excel are equal";
        String FailureMessage = "Banner Validation Failed";
        String FailureComments = "Banner in web and Banner in excel are not equal";
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BannerValidation_MethodNameKey, Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BannerValidation_MethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BannerValidation_MethodNameKey, ExcelMethodInput.Test_Description_Key);
        String banner = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.BannerValidation_MethodNameKey,
        BannerurlKey);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BannerValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BannerValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.BannerValidation_MethodNameKey, ExcelMethodInput.Actual_Result_Key);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Banner Validation= " + executeYesOrNo);
            LOGGER.info("Banner in Excel: " + banner);
            Thread.sleep(5000);
            WebElement googleSearchBtn = driver.findElement(By.className(WebElement));
            String webbanner = googleSearchBtn.getAttribute("data-bg");
            LOGGER.info("Banner In Web: " + webbanner);
            Thread.sleep(5000);
            // * Checking Area
            if (webbanner.equals(banner)) {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                resultMap.put(TestResult.R_ACTUAL_RESULT, "");
               
            }
            // * Common Parameter
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            // * Other Return Required Parameter
            resultMap.put(BannerurlKey, banner);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            LOGGER.info("resultMap= " + resultMap);
            LOGGER.info("MethodExecuteOrNot Validation= " + testMethodNameTestNG);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
    }
    @Test
    public void MoreDetailsAndLessDetail(ITestContext testContext) {
        String testMethodNameTestNG = "MoreDetailsAndLessDetail";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "More Details And Less Detail";
        String Web_Element = "Web Element";
        String Web_Element_View = "Web Element View";
        String SuccessMessage = "Check the More Details and Less Detail Working";
        String SuccessComments = "Check the More Details and Less Detail Working";
        String FailureMessage = "LessDetail Present Not Working";
        String FailureComments = "LessDetail Not Working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Less_DetailMethodNameKey,
        Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Less_DetailMethodNameKey,
        ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Less_DetailMethodNameKey,
        ExcelMethodInput.Test_Description_Key);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Less_DetailMethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Less_DetailMethodNameKey,
        ExcelMethodInput.Actual_Result_Key);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Less_DetailMethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        LOGGER.info("case= " + executeYesOrNo);
        LOGGER.info("WebElement= " + WebElement);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Less Detail Validation= " + executeYesOrNo);
            Thread.sleep(5000);
            driver.findElement(By.className(WebElement)).click();
            Thread.sleep(4000);
            WebElement elements = driver.findElement(By.className(WebElement));
            Thread.sleep(3000);
            // Checking Areas
            if (elements.isDisplayed()) {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            }
            // * Common Parameter
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(ExcelMethodInput.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(ExcelMethodInput.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
    }
    @Test
    public void ZCompare(ITestContext testContext) throws InterruptedException {
        String testMethodNameTestNG = "ZCompare";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Compare Validation";
        String WebCompareButton1 = "Web Element in compare Button 1";
        String WebCompareButton2 = "Web Element in compare Button 2";
        String WebCompareNowButton = "Web Element in compare Button Now Button";
        String WebClearShortlist = "Clear ShortList";
        // * Custom messages
        String SuccessMessage = "Compare Button Validation Passed";
        String SuccessComments = "Compare Button working successfully";
        String FailureMessage = "Compare Button Validation Failed";
        String FailureComments = "Compare Button is not working";
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String WebElement1 = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.CompareValidation_MethodNameKey, WebCompareButton1);
        String WebElement2 = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.CompareValidation_MethodNameKey, WebCompareButton2);
        String WebCompareNowButtons = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.CompareValidation_MethodNameKey, WebCompareNowButton);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.CompareValidation_MethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.CompareValidation_MethodNameKey, ExcelMethodInput.Test_Description_Key);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.CompareValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.CompareValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.CompareValidation_MethodNameKey, ExcelMethodInput.Actual_Result_Key);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Compare Validation= " + executeYesOrNo);
            driver.get(pageUrl);
            Thread.sleep(8000);
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,100)");
            driver.findElement(By.xpath(WebElement1)).click();
            Thread.sleep(1000);
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,600)");
            Thread.sleep(2000);
            driver.findElement(By.xpath(WebElement2)).click();
            Thread.sleep(2000);
            WebElement compareButton = driver.findElement(By.id(WebCompareNowButtons));
            Thread.sleep(2000);
            // * Checking Area
            if (compareButton.isDisplayed()) {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                LOGGER.info("Compare Button is validated Failed");
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            }
            // * Common Parameter
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            LOGGER.info("resultMap= " + resultMap);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            
            driver.navigate().to(pageUrl);
            Thread.sleep(10000);
            // * Other Return Required Parameter
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            LOGGER.info("resultMap= " + resultMap);
            LOGGER.info("MethodExecuteOrNot Validation= " + testMethodNameTestNG);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
    }
    @Test
    public void ZApplyNow(ITestContext testContext) throws InterruptedException {
        String window1 = "";
        String testMethodNameTestNG = "ZApplyNow";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Apply Now";
        String Web_Element = "Web Element";
        String Apply_Now_URL = "Apply Now URL";
        String SuccessMessage = "Apply Now Working";
        String SuccessComments = "Apply Now Present and Working";
        String FailureMessage = "Apply Now Present Not Working";
        String FailureComments = "Apply Now Not Working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Apply_Now_Method_NameKey,
        Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Apply_Now_Method_NameKey,
        ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Apply_Now_Method_NameKey,
        ExcelMethodInput.Test_Description_Key);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Apply_Now_Method_NameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Apply_Now_Method_NameKey,
        ExcelMethodInput.Actual_Result_Key);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Apply_Now_Method_NameKey,
        TestResult.R_METHOD_EXECUTE_OR_NOT);
        String ApplyNowURL = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Apply_Now_Method_NameKey,
        Apply_Now_URL);
        LOGGER.info("case= " + executeYesOrNo);
        LOGGER.info("ApplyNowURL = " + ApplyNowURL);
        LOGGER.info("ApplyNowURL WebElement = " + WebElement);
        LOGGER.info("ApplyNowURL WebElement channelName = " + channelName);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Apply Now Validation= " + executeYesOrNo);
            driver.navigate().to(pageUrl);
            Thread.sleep(10000);
            driver.findElement(By.xpath(WebElement)).click();
            Thread.sleep(9000);
            if (!channelName.contains("Car Loan")) {
                LOGGER.info("FIRST = " + channelName);
                if (!channelName.contains("multi-purpose-loan")) {
                    LOGGER.info("SECOND = " + channelName);
                    if (!channelName.contains("auto-loans")) {
                        LOGGER.info("Third = " + channelName);
                        Set<String> AllWindowHandles = driver.getWindowHandles();
                        window1 = (String) AllWindowHandles.toArray()[0];
                        
                        String window2 = (String) AllWindowHandles.toArray()[1];
                        
                        driver.switchTo().window(window2);
                        Thread.sleep(10000);
                    }
                }
            }
            String url = driver.getCurrentUrl();
            LOGGER.info("n Current Page URl:" + url);
            Thread.sleep(1000);
            if (url.contains(ApplyNowURL)) {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                Assert.fail(FailureMessage);
            }
            // * Common Parameter
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(ExcelMethodInput.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(ExcelMethodInput.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
        driver.switchTo().window(window1);
        Thread.sleep(10000);
    }
    @Test
    public void ShowMoreResult(ITestContext testContext) throws InterruptedException {
        String window1 = "";
        String testMethodNameTestNG = "ShowMoreResult";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Show More Result";
        String Web_Element = "Web Element for Show More Result Button";
        String Web_Element_Apply = "Web Element for Apply Now Button";
        String Apply_Now_URL = "Apply Now URL";
        String SuccessMessage = "Apply Now Working";
        String SuccessComments = "Apply Now Present and Working";
        String FailureMessage = "Apply Now Present Not Working";
        String FailureComments = "Apply Now Not Working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.ShowMoreResultMethodNameKey,
        Web_Element);
        String Apply_Now_Page_URL = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.ShowMoreResultMethodNameKey, Apply_Now_URL);
        String WebElementApply = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.ShowMoreResultMethodNameKey, Web_Element_Apply);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.ShowMoreResultMethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.ShowMoreResultMethodNameKey, ExcelMethodInput.Test_Description_Key);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.ShowMoreResultMethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.ShowMoreResultMethodNameKey, ExcelMethodInput.Actual_Result_Key);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.ShowMoreResultMethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        LOGGER.info("case= " + executeYesOrNo);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Apply Now Validation= " + executeYesOrNo);
            Thread.sleep(6000);
            WebElement firstname = driver.findElement(By.xpath(WebElement));
            if (firstname.isEnabled()) {
               
                Thread.sleep(6000);
                driver.findElement(By.xpath(WebElement)).click();
                Thread.sleep(18000);
                driver.findElement(By.xpath(WebElementApply)).click();
                Thread.sleep(10000);
                Set<String> AllWindowHandles = driver.getWindowHandles();
                window1 = (String) AllWindowHandles.toArray()[0];
             
                String window2 = (String) AllWindowHandles.toArray()[1];
               
                driver.switchTo().window(window2);
                Thread.sleep(10000);
                String url = driver.getCurrentUrl();
                LOGGER.info("n Current Page URl:" + url);
                Thread.sleep(10000);
                if (url.contains(Apply_Now_Page_URL)) {
                    resultMap.put(TestResult.R_IS_SUCCESS, true);
                    resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                    resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                    resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                    } else {
                    resultMap.put(TestResult.R_IS_SUCCESS, false);
                    resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                    resultMap.put(TestResult.R_COMMENTS, FailureComments);
                    resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                    Assert.fail(FailureMessage);
                }
                } else {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_DESRIPTION, description);
                resultMap.put(TestResult.R_MESSAGE, "Load More Button is Disabled and all Result Loaded");
                resultMap.put(TestResult.R_COMMENTS, "Load More Button is Disabled and all Result Loaded");
                resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            }
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            // * Common Parameter
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(ExcelMethodInput.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(ExcelMethodInput.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
        driver.switchTo().window(window1);
        Thread.sleep(10000);
    }
    @Test
    public void SponsoredImageValidation(ITestContext testContext) {
        String testMethodNameTestNG = "SponsoredImageValidation";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Sponsored Image";
        String SponsoredImageurlKey = "Image Url";
        String Web_Element = "Web Element";
        // * Custom messagess
        String SuccessMessage = "Sponsored Image Present in the Website and Validated";
        String SuccessComments = "Sponsored Image in web and Banner in excel are equal";
        String FailureMessage = "Sponsored Image Validation Failed";
        String FailureComments = "Sponsored Image in web and Sponsored Image in excel are not equal";
        String WebElement = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.SponsoredImageValidation_MethodNameKey, Web_Element);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.SponsoredImageValidation_MethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.SponsoredImageValidation_MethodNameKey, ExcelMethodInput.Test_Description_Key);
        String SponsoredImage = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.SponsoredImageValidation_MethodNameKey, SponsoredImageurlKey);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.SponsoredImageValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.SponsoredImageValidation_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.SponsoredImageValidation_MethodNameKey, ExcelMethodInput.Actual_Result_Key);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Sponsored Image Validation= " + executeYesOrNo);
            LOGGER.info("Sponsored Image in Excel: " + SponsoredImage);
            Thread.sleep(8000);
            WebElement googleSearchBtn = driver.findElement(By.className(WebElement));
            String webSponsoredImage = googleSearchBtn.getAttribute("data-bg");
            LOGGER.info("Sponsored Image In Web: " + webSponsoredImage);
            Thread.sleep(5000);
            // * Checking Area
            if (webSponsoredImage.equals(SponsoredImage)) {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                resultMap.put(TestResult.R_ACTUAL_RESULT, "");
            }
            // * Common Parameter
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            LOGGER.info("MethodExecuteOrNot Validation= " + MethodExecuteOrNot);
            LOGGER.info("resultMap= " + resultMap);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            // * Other Return Required Parameter
            resultMap.put(SponsoredImageurlKey, SponsoredImage);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            LOGGER.info("resultMap= " + resultMap);
            LOGGER.info("MethodExecuteOrNot Validation= " + testMethodNameTestNG);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
    }
    // * Check DoTestmonial Validation
    @Test
    public void Testmonial(ITestContext testContext) {
        String testMethodNameTestNG = "Testmonial";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        // * Excel Keys
        String MethodNameKey = "Testimonial";
        String TestimonialImage = "Testimonial Image";
        String TestimonialDecription = "Testimonial Decription";
        String CutomerName = "Cutomer Name";
        String Expected_Result_Key = "Expected Test Result";
        String Test_Description_Key = "Test Description";
        String CustomerDesignation = "Customer Designation";
        String Actual_Result_Key = "Actual Result";
        // * Custom messages
        String SuccessMessage = "Testimonial Present in the Website ";
        String SuccessComments = "Testimonial in web and Banner in excel are equal";
        String FailureMessage = "Testimonial Validation Failed";
        String FailureComments = "Testimonial in web and Banner in excel are not equal";
        String ParentCalssForMoneySmarSite = "//section[contains(@class,'testimonials')]";
        String ChildOneCalssForParentCalss = "testimonial";
        String Webelementdesc = "desc";
        String MethodExecuteOrNotMultiple = "";
        String SuccessActualResult = "";
        String FailureActualResult = "";
        String Web_Element = "Web Element";
        List<String> TestimonialListFromWebWithoutDuplicate;
        boolean isAllTestimonialsValidatedAndSucess = true;
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Testimonial_MethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Testimonial_MethodNameKey,
        ExcelMethodInput.Test_Description_Key);
        String TestimonialImg = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Testimonial_MethodNameKey, TestimonialImage);
        String CutomerNam = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Testimonial_MethodNameKey,
        CutomerName);
        String TestimonialDecs = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Testimonial_MethodNameKey, TestimonialDecription);
        String CustomerDesign = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Testimonial_MethodNameKey, CustomerDesignation);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Testimonial_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Testimonial_MethodNameKey,
        ExcelMethodInput.Actual_Result_Key);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Testimonial_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Testimonial Validation= " + executeYesOrNo);
            Thread.sleep(10000);
            LOGGER.info("Testimonial Image in Excel: " + TestimonialImg);
            Thread.sleep(8000);
            // * Test
            ExcelInputData newexcelInputData = ExcelInputData.getInstance();
            GetExcelInput getInput1 = new GetExcelInput();
            ArrayList<HashMap<String, ExcelTestimonialData>> testimonialsInExcel = newexcelInputData
            .getExcelTestimonialInputData();
            WebElement element = driver.findElement(By.xpath(ParentCalssForMoneySmarSite));
            List<WebElement> contentList = element.findElements(By.className(ChildOneCalssForParentCalss));
            LOGGER.info("Testimonial check desc = " + contentList.size());
            LOGGER.info("contentList = " + contentList);
            LOGGER.info("element = " + element);
            LOGGER.info("Webelementdesc = " + Webelementdesc);
            String firstTEstimonial = MethodNameKey + " 1";
            Webelementdesc = getInput.get_A_Value_Using_Key_Of_A_Method(firstTEstimonial, "Web Element");
            LOGGER.info("Webelementdesc = " + Webelementdesc);
            TestimonialListFromWebWithoutDuplicate = setProductNameInCollectionAndRemoveDuplicateinTestimonial(
            contentList, Webelementdesc);
            int testimonialCountinExcel = testimonialsInExcel.size();
            int testimonialCountinWeb = TestimonialListFromWebWithoutDuplicate.size();
            if (testimonialCountinExcel != testimonialCountinWeb) {
                isAllTestimonialsValidatedAndSucess = false;
                FailureActualResult = "There are " + testimonialCountinExcel + " testimonials in excel. But "
                + testimonialCountinWeb + "n";
            }
            for (int testimonialCount = 0; testimonialCount < testimonialsInExcel.size(); testimonialCount++) {
                int testiCont = testimonialCount + 1;
                String methodnameExcel = MethodNameKey + " " + String.valueOf(testiCont);
                if (expecedResult.length() > 0) {
                    expecedResult = expecedResult + ", "
                    + getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, Expected_Result_Key);
                    description = description + ", "
                    + getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, Test_Description_Key);
                    TestimonialImg = TestimonialImg + ", "
                    + getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, TestimonialImage);
                    CutomerNam = CutomerNam + ", "
                    + getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, CutomerName);
                    TestimonialDecs = TestimonialDecs + ", "
                    + getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, TestimonialDecription);
                    CustomerDesign = CustomerDesign + ", "
                    + getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, CustomerDesignation);
                    MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel,
                    TestResult.R_METHOD_EXECUTE_OR_NOT);
                    MethodExecuteOrNotMultiple = MethodExecuteOrNotMultiple
                    + ", "
                    + getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel,
                    TestResult.R_METHOD_EXECUTE_OR_NOT);
                    actualResult = actualResult + ", "
                    + getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, Actual_Result_Key);
                    } else {
                    expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, Expected_Result_Key);
                    description = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, Test_Description_Key);
                    TestimonialImg = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, TestimonialImage);
                    CutomerNam = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, CutomerName);
                    TestimonialDecs = getInput
                    .get_A_Value_Using_Key_Of_A_Method(methodnameExcel, TestimonialDecription);
                    CustomerDesign = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, CustomerDesignation);
                    MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel,
                    TestResult.R_METHOD_EXECUTE_OR_NOT);
                    MethodExecuteOrNotMultiple = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel,
                    TestResult.R_METHOD_EXECUTE_OR_NOT);
                    actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(methodnameExcel, Actual_Result_Key);
                }
                String testimonialDescWebAndAPI = "";
                Map<String, ExcelTestimonialData> singleTestimonial = testimonialsInExcel.get(testimonialCount);
                LOGGER.info("singleTestimonial  " + singleTestimonial);
                String testimonialDescriptionexcel = getInput1.get_A_Value_Using_Key_Of_Testimonial_Method(
                TestResult.R_TESTIMONIAL + " " + testiCont, TestimonialDecription);
                String testimonialDescriptionweb = TestimonialListFromWebWithoutDuplicate.get(testimonialCount);
                LOGGER.info("Testimonial " + testiCont + " description Excel= " + testimonialDescriptionexcel);
                LOGGER.info("Testimonial " + testiCont + " description  Web= " + testimonialDescriptionweb);
                // * Test
                if (testimonialDescriptionweb.trim().contains(testimonialDescriptionexcel.trim())) {
                    LOGGER.info("Testimonial Desc True: ");
                    SuccessActualResult = "There are " + testimonialCountinExcel + " testimonials in excel and "
                    + testimonialCountinWeb + " testimonials in website." + "n";
                    SuccessActualResult = SuccessActualResult
                    + " All testimonial descriptions are matching with Excel input";
                    } else {
                    LOGGER.info("Testimonial Desc false: Failed");
                    isAllTestimonialsValidatedAndSucess = false;
                    LOGGER.info("Testimonial : " + testimonialDescriptionexcel.trim());
                    LOGGER.info("Testimonial : " + testimonialDescriptionweb.trim());
                    testimonialDescWebAndAPI = testimonialDescriptionweb.trim() + ", "
                    + testimonialDescriptionexcel.trim();
                    FailureActualResult = FailureActualResult + "(" + testimonialDescWebAndAPI
                    + ") This description is not matching with Excel description, ";
                    FailureActualResult = FailureActualResult + " for Testimonial " + testiCont + " n";
                }
            }
            // * Checking Area
            if (isAllTestimonialsValidatedAndSucess) {
                LOGGER.info("All testimonials description are passed" + SuccessActualResult);
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, SuccessActualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                LOGGER.info("May be some testimonials description failed" + FailureActualResult);
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, FailureActualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            }
            // * Common Parameter
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(TestResult.R_MULTIPLE_EXECUTE_FOR_TESTIMONIAL, MethodExecuteOrNotMultiple);
            // * Other Return Required Parameter
            resultMap.put(ExcelMethodInput.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(ExcelMethodInput.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TestimonialImage, TestimonialImg);
            resultMap.put(CutomerName, CutomerNam);
            resultMap.put(TestimonialDecription, TestimonialDecs);
            resultMap.put(CustomerDesignation, CustomerDesign);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            Assert.fail(e.toString());
        }
    }
    public List<String> setProductNameInCollectionAndRemoveDuplicateinTestimonial(List<WebElement> contentList,
    String webElement) {
        List<String> webProdArray = new ArrayList<String>();
        for (WebElement content : contentList) {
            String productNameWeb = content.findElement(By.className(webElement)).getText();
            webProdArray.add(productNameWeb);
            LOGGER.info("Testimonial check desc = " + productNameWeb);
        }
        // add elements to al, including duplicates
        Set<String> hs = new HashSet<>();
        hs.addAll(webProdArray);
        webProdArray.clear();
        webProdArray.addAll(hs);
        return webProdArray;
    }
    @Test
    public void faqtabvalidation(ITestContext testContext) throws InterruptedException {
        String testMethodNameTestNG = "faqtabvalidation";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "FAQ";
        String Web_Element = "Web Element";
        String FAQ_Link_Web_Element = "Web Element for FAQ";
        String SuccessMessage = "FAQ Tab  Validation Passed";
        String SuccessComments = "FAQ Tab navigation is working";
        String FailureMessage = "FAQ Tab  Validation Failed";
        String FailureComments = "FAQ Tab navigation is not working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.FAQ_MethodNameKey,
        ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.FAQ_MethodNameKey,
        ExcelMethodInput.Test_Description_Key);
        String frequentlyAskedQuestions = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.FAQ_MethodNameKey, Web_Element);
        String faqLink = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.FAQ_MethodNameKey,
        FAQ_Link_Web_Element);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.FAQ_MethodNameKey,
        TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.FAQ_MethodNameKey,
        ExcelMethodInput.Actual_Result_Key);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.FAQ_MethodNameKey,
        TestResult.R_METHOD_EXECUTE_OR_NOT);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo faq tab Validation= " + executeYesOrNo);
            Thread.sleep(6000);
            driver.findElement(By.xpath(faqLink)).click();
            Thread.sleep(6000);
            String FAQ = driver.findElement(By.xpath(frequentlyAskedQuestions)).getText();
            if (FAQ.contains("Frequently Asked Questions")) {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            }
            Thread.sleep(8000);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(ExcelMethodInput.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(ExcelMethodInput.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
        driver.navigate().to(pageUrl);
        Thread.sleep(8000);
    }
    // * Check ask question Validation
    @Test
    public void askquestion(ITestContext testContext) throws InterruptedException {
        String testMethodNameTestNG = "askquestion";
        LOGGER.info("Start the test method = " + testMethodNameTestNG);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        GetExcelInput getInput = new GetExcelInput();
        String MethodNameKey = "Ask a Question";
        String Web_Element = "Web Element";
        String ASK_Link_Web_Element = "Web Element for ASK";
        String SuccessMessage = "Ask a Question Tab  Validation Passed";
        String SuccessComments = "Ask a Question Tab navigation is working";
        String FailureMessage = "Ask a Question Tab  Validation Failed";
        String FailureComments = "Ask a Question Tab navigation is not working";
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChanellURlKey);
        String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Ask_A_Question_MethodNameKey, ExcelMethodInput.Expected_Result_Key);
        String description = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Ask_A_Question_MethodNameKey, ExcelMethodInput.Test_Description_Key);
        String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Ask_A_Question_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Ask_A_Question_MethodNameKey, ExcelMethodInput.Actual_Result_Key);
        String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Ask_A_Question_MethodNameKey, TestResult.R_METHOD_EXECUTE_OR_NOT);
        String AskedQuestions = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.Ask_A_Question_MethodNameKey, Web_Element);
        String askLink = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Ask_A_Question_MethodNameKey,
        ASK_Link_Web_Element);
        LOGGER.info("askLinkaskLinkaskLink= " + askLink);
        if (!executeYesOrNo.equalsIgnoreCase("Y")) {
            throw new SkipException("This method skipped");
        }
        try {
            s_assert = new SoftAssert();
            LOGGER.info("executeYesOrNo Ask a Question Validation= " + executeYesOrNo);
            Thread.sleep(9000);
            driver.findElement(By.cssSelector(askLink)).click();
            Thread.sleep(9000);
            String ask = driver.findElement(By.xpath(AskedQuestions)).getText();
            LOGGER.info("askaskaskaskask " + ask);
            if (ask.contains("Ask MoneySmart")) {
                resultMap.put(TestResult.R_IS_SUCCESS, true);
                resultMap.put(TestResult.R_MESSAGE, SuccessMessage);
                resultMap.put(TestResult.R_COMMENTS, SuccessComments);
                resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                } else {
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, FailureMessage);
                resultMap.put(TestResult.R_COMMENTS, FailureComments);
                resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
                Assert.fail(FailureMessage);
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
            resultMap.put(ExcelMethodInput.Openwebpage_ChaneelNameKey, channelName);
            resultMap.put(ExcelMethodInput.Openwebpage_ChanellURlKey, pageUrl);
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            boolean isMethodSuccess = (boolean) resultMap.get(TestResult.R_IS_SUCCESS);
            String resultMessage = (String) resultMap.get(TestResult.R_MESSAGE);
            if (!isMethodSuccess) {
                if (executeYesOrNo.equalsIgnoreCase("Y")) {
                    Assert.fail(resultMessage);
                }
            }
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            } catch (Exception e) {
            if (resultMap.isEmpty()) {
                resultMap = new HashMap<String, Object>();
            }
            resultMap.put(TestResult.R_METHOD_NAME, MethodNameKey);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expecedResult);
            resultMap.put(TestResult.R_DESRIPTION, description);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualResult);
            testContext.setAttribute(testMethodNameTestNG, resultMap);
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
        driver.navigate().to(pageUrl);
        Thread.sleep(9000);
    }
}
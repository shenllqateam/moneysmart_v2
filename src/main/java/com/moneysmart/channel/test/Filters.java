package com.moneysmart.channel.test;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.model.Constants.TestResult;
import com.model.ExcelCalculationInputData;
import com.model.ExcelFilterInputData;
import com.model.ExcelInputData;
import com.model.GetExcelInput;
import com.testing.utility.GetWebElement;

import java.util.logging.Level;
import java.util.logging.Logger;
public class Filters {
    private static final Logger LOGGER = Logger.getLogger(Filters.class.getName());
    private String exceptionString = "EXCEPTION";
    int filterMethodInvocationCount = 0;
    GetExcelInput excelInputInstance = null;
    String locatorNameForNLFFILtercalculateButton = "calculate-btn-1";
    String locatorNameForNLFFILterFieldValidation = "validation-message";
    String actualFilterFailureResult = "";
    boolean allMandatoryValidationPassed = true;
    boolean allMinimumValidationPassed = true;
    boolean allMaximunValidationPassed = true;
    String expectedResultExcel = "";
    String testDescriptionExcel = "";
    String actualResultExcel = "";
    String executeOrNotExcel = "";
    @Test
    public void DoNLFfilters(ITestContext testContext) {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        String TestMethodName = "DoNLFfilters";
        String TestMethodNameReport = "Filter Validation";
        // String filterMethodNameKey = "";
        try {
            getWebDriver().navigate().refresh();
            Thread.sleep(10000);
            initExcelInputInstacne();
            // Loop the MIN,MAX,MANDATORY condition for all input for single
            // filter
            String currentInputNameFilter = getCurrentMethodName(filterMethodInvocationCount);
            LOGGER.info("FILTER NAME : " + currentInputNameFilter);
            expectedResultExcel = getValueFromExcel(currentInputNameFilter, "Expected Test Result");
            testDescriptionExcel = getValueFromExcel(currentInputNameFilter, "Test Description");
            actualResultExcel = getValueFromExcel(currentInputNameFilter, "Actual Result");
            executeOrNotExcel = getValueFromExcel(currentInputNameFilter, "Execute");
            if (!executeOrNotExcel.equalsIgnoreCase("Y")) {
                throw new SkipException("Filter validation skippped for Filter: " + currentInputNameFilter);
            }
            int totalCalcFilterInputCount = getTotalCalculationCountForSingleFilter(currentInputNameFilter);
            if (totalCalcFilterInputCount > 1) {
                for (int calcInputCount = 1; calcInputCount <= totalCalcFilterInputCount - 1; calcInputCount++) {
                    String calcInputValue = getCurrentCalculationInputForCurrentFilter(currentInputNameFilter,
                    calcInputCount);
                    LOGGER.info("FILTER CALCULATION VALUE : " + calcInputValue);
                    Thread.sleep(5000);
                    // Minimum Value Checking
                    if (isMinimumAvailable()) {
                        // getLocatorName()
                        if (!doMinimumValidation(getFilterWebElement(), getLocator(), calcInputValue)) {
                            allMinimumValidationPassed = false;
                            // store your message in the below string that
                            // maximum is failed for some reaseon for the
                            // getCurrentFilt
                            createValidationFailureMesage("Minimum", calcInputCount);
                            // Maximum Validation condition check if Minimum
                            // value failed...
                            Thread.sleep(5000);
                            Integer minValue = Integer.parseInt(getMinimumValue());
                            // Replace , separator in hte below line..
                            String calcvalue = calcInputValue.replace(",", "");
                            Integer calcValueInt = Integer.parseInt(calcvalue);
                            if (calcValueInt > minValue) {
                                // Maximum Value Checking
                                if (isMaximumAvailable()) {
                                    if (!doMaximumValidation(getFilterWebElement(), getLocator(), calcInputValue, true)) {
                                        allMaximunValidationPassed = false;
                                        // store your message in the below
                                        // string that maximum is failed for
                                        // some reaseon for the getCurrentFilt
                                        createValidationFailureMesage("Maximum", calcInputCount);
                                        }
                                    } 
                                } else {
                                LOGGER.info("Minimum Value is failed and calculation value is less than minimum value. So skipped maximum validation condition ");
                            }
                            } 
                        } 
                    // Maximum Value Checking
                    if (allMinimumValidationPassed && isMaximumAvailable()) {
                        if (!doMaximumValidation(getFilterWebElement(), getLocator(), calcInputValue, false)) {
                            allMaximunValidationPassed = false;
                            // store your message in the below string that
                            // maximum is failed for some reaseon for the
                            // getCurrentFilt
                            createValidationFailureMesage("Maximum", calcInputCount);
                            } 
                        } 
                    // One filter going to complete....
                    // / Set required result Map here
                }
                Thread.sleep(2000);
                // Mandatory Checking for only one time for a filter
                if (isMandatoryAvailable()) {
                    if (!doMandatoryValidation(getFilterWebElement(), getLocator(),
                    getCurrentCalculationInputForCurrentFilter(currentInputNameFilter, 1))) {
                        allMandatoryValidationPassed = false;
                        // store your message in the below string that maximum
                        // is failed for some reaseon for the getCurrentFilt
                        createValidationFailureMesage("Mandatory", 0);
                        } else {
                        if (!allMandatoryValidationPassed) {
                            allMandatoryValidationPassed = false;
                        }
                    }
                    } else {
                    if (!allMandatoryValidationPassed) {
                        allMandatoryValidationPassed = false;
                    }
                    // mandatory not available.. so no need to check mandatory
                    // checking
                }
                if (filterMethodInvocationCount == getTotalFilterMethodCount() - 1) {
                    // if(doClickCalculateButton()){
                        if (!allMandatoryValidationPassed || !allMinimumValidationPassed || !allMaximunValidationPassed) {
                            // send the - actualFilterFailureResult - to report and
                            // mark this method as failed
                            LOGGER.info("allMandatoryValidationPassed = " + allMandatoryValidationPassed);
                            LOGGER.info("allMinimumValidationPassed = " + allMinimumValidationPassed);
                            LOGGER.info("allMaximunValidationPassed = " + allMaximunValidationPassed);
                            LOGGER.info("FILTER VALIDATION FAILED");
                            LOGGER.info("RESULT : " + actualFilterFailureResult);
                            resultMap.put(TestResult.R_IS_SUCCESS, false);
                            resultMap.put(TestResult.R_MESSAGE, actualFilterFailureResult);
                            resultMap.put(TestResult.R_COMMENTS, actualFilterFailureResult);
                            } else {
                            // if you reached this area,, all filter validations are
                            // passed
                            LOGGER.info("ALL FILTER VALIDATIONS ARE PASSED");
                            actualFilterFailureResult = actualResultExcel;
                            resultMap.put(TestResult.R_IS_SUCCESS, true);
                            resultMap.put(TestResult.R_MESSAGE, actualFilterFailureResult);
                            resultMap.put(TestResult.R_COMMENTS, actualFilterFailureResult);
                            // Set All
                        }
                    // }
                }
                } else {
                //
                actualFilterFailureResult = "No calculation input for testing. Please review the excel and provide atleast one calculation input for testing.";
                resultMap.put(TestResult.R_IS_SUCCESS, false);
                resultMap.put(TestResult.R_MESSAGE, actualFilterFailureResult);
                resultMap.put(TestResult.R_COMMENTS, actualFilterFailureResult);
            }
            // Set Common
            /*
            * Common Parameter
            */
            resultMap.put(TestResult.R_METHOD_NAME, TestMethodNameReport);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expectedResultExcel);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualFilterFailureResult);
            resultMap.put(TestResult.R_DESRIPTION, testDescriptionExcel);
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeOrNotExcel);
            testContext.setAttribute(TestMethodName, resultMap);
            } catch (Exception e) {
            // TODO Auto-generated catch block
            resultMap.put(TestResult.R_METHOD_NAME, TestMethodNameReport);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, TestResult.R_IS_EXCEPTION);
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, e.toString());
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, executeOrNotExcel);
            resultMap.put(TestResult.R_EXPECTED_RESULT, expectedResultExcel);
            resultMap.put(TestResult.R_DESRIPTION, testDescriptionExcel);
            resultMap.put(TestResult.R_ACTUAL_RESULT, actualFilterFailureResult);
            testContext.setAttribute(TestMethodName, resultMap);
            LOGGER.info("Failure method name = " + resultMap.get(TestResult.R_METHOD_NAME));
            LOGGER.log(Level.SEVERE, exceptionString, e);
            Assert.fail(e.toString());
        }
        filterMethodInvocationCount++;
    }
    /*
    * Validation Helper Methods
    */
    public boolean doMandatoryValidation(WebElement webElement, String filtercomponentType, String calculationInput)
    throws Exception {
        boolean isMandatoryValidationPassed = false;
        // DO YOUR WORK HERE
        if (filtercomponentType.equals("textbox")) {
            webElement.clear();
            webElement.click();
        }
        if (!isCalculateButtonAllowed("Mandatory")) {
            isMandatoryValidationPassed = true;
        }
        webElement.sendKeys(calculationInput);
        return isMandatoryValidationPassed;
    }
    public boolean isCalculateButtonAllowed(String clickedFor) throws Exception {
        boolean isCalculateButtonAllowed = false;
        WebElement calculateButton = getWebDriver().findElement(By.id(locatorNameForNLFFILtercalculateButton));
        calculateButton.click();
        if (!calculateButton.isDisplayed()) {
            isCalculateButtonAllowed = true;
            LOGGER.info(clickedFor + " : Calculate button hided");
            } else {
            LOGGER.info(clickedFor + " : Calculate button not hided");
        }
        return isCalculateButtonAllowed;
    }
    public boolean doMinimumValidation(WebElement webElement, String filtercomponentType, String calculationInput)
    throws Exception {
        boolean isMinimumValidationPassed = false;
        // DO YOUR WORK HERE
        if (filtercomponentType.equals("textbox")) {
            webElement.clear();
            webElement.sendKeys(calculationInput);
        }
        Integer minValue = Integer.parseInt(getMinimumValue());
        // Replace , separator in hte below line..
        calculationInput = calculationInput.replace(",", "");
        Integer calcValue = Integer.parseInt(calculationInput);
        if (calcValue > minValue) {
            isMinimumValidationPassed = true;
        }
        return isMinimumValidationPassed;
    }
    public boolean doMaximumValidation(WebElement webElement, String webElementType, String calculationInput,
    boolean isMinimumValidationFailed) throws Exception {
        boolean isMaximumValidationPassed = false;
        // DO YOUR WORK HERE
        if (webElementType.equals("textbox")) {
            webElement.clear();
            webElement.sendKeys(calculationInput);
        }
        // Replace , separator in hte below line..
        calculationInput = calculationInput.replace(",", "");
        Integer calcValue = Integer.parseInt(calculationInput);
        Integer maxValue = Integer.parseInt(getMaximumValue());
        // LOGGER.info("Max : calcValue "+calcValue+ " < "+maxValue);
        if (calcValue <= maxValue) {
            if (!isMinimumValidationFailed) {
                if (isCalculateButtonAllowed("Maximum")) {
                    isMaximumValidationPassed = true;
                }
                } else {
                isMaximumValidationPassed = true;
            }
        }
        return isMaximumValidationPassed;
    }
    /*
    * Initialiser Methods
    */
    public void initExcelInputInstacne() throws Exception {
        if (this.excelInputInstance == null) {
            this.excelInputInstance = new GetExcelInput();
        }
    }
    public void createValidationFailureMesage(String condition, int calcInputCount) throws Exception {
        if (this.actualFilterFailureResult != null) {
            if (this.actualFilterFailureResult.length() == 0) {
                this.actualFilterFailureResult = " *** FILTER VALIDATION FAILED *** " + " FILTER NAME: "
                + getCurrentMethodName(filterMethodInvocationCount) + " FAILED : " + condition + " Validation";
                if (!condition.equals("Mandatory") && condition.equals("Minimum")) {
                    this.actualFilterFailureResult = this.actualFilterFailureResult
                    + " for input "
                    + getCurrentCalculationInputForCurrentFilter(
                    getCurrentMethodName(filterMethodInvocationCount), calcInputCount)
                    + ". Because minimum value is " + getMinimumValue();
                    } else if (!condition.equals("Mandatory") && condition.equals("Maximum")) {
                    this.actualFilterFailureResult = this.actualFilterFailureResult
                    + " for input "
                    + getCurrentCalculationInputForCurrentFilter(
                    getCurrentMethodName(filterMethodInvocationCount), calcInputCount)
                    + ". Because maximum value is " + getMaximumValue();
                }
                } else {
                this.actualFilterFailureResult = this.actualFilterFailureResult + " , " + " FILTER NAME: "
                + getCurrentMethodName(filterMethodInvocationCount) + " FAILED : " + condition + " Validation";
                if (!condition.equals("Mandatory") && condition.equals("Minimum")) {
                    this.actualFilterFailureResult = this.actualFilterFailureResult
                    + " for input "
                    + getCurrentCalculationInputForCurrentFilter(
                    getCurrentMethodName(filterMethodInvocationCount), calcInputCount)
                    + ". Because minimum value is " + getMinimumValue();
                    } else if (!condition.equals("Mandatory") && condition.equals("Maximum")) {
                    this.actualFilterFailureResult = this.actualFilterFailureResult
                    + " for input "
                    + getCurrentCalculationInputForCurrentFilter(
                    getCurrentMethodName(filterMethodInvocationCount), calcInputCount)
                    + ". Because maximum value is " + getMaximumValue();
                }
            }
            } else {
            this.actualFilterFailureResult = " *** FILTER VALIDATION FAILED *** " + " FILTER NAME: "
            + getCurrentMethodName(filterMethodInvocationCount) + " FAILED : " + condition + " Validation";
            if (!condition.equals("Mandatory") && condition.equals("Minimum")) {
                this.actualFilterFailureResult = this.actualFilterFailureResult
                + " for input "
                + getCurrentCalculationInputForCurrentFilter(getCurrentMethodName(filterMethodInvocationCount),
                calcInputCount) + ". Because minimum value is " + getMinimumValue();
                } else if (!condition.equals("Mandatory") && condition.equals("Maximum")) {
                this.actualFilterFailureResult = this.actualFilterFailureResult
                + " for input "
                + getCurrentCalculationInputForCurrentFilter(getCurrentMethodName(filterMethodInvocationCount),
                calcInputCount) + ". Because maximum value is " + getMaximumValue();
            }
        }
    }
    /*
    * Other Helper Methods
    */
    public boolean isValidationMessageDisplay(String validationFor) throws Exception {
        boolean isValidationMessageDisplay = false;
        WebElement elements = getWebDriver().findElement(By.className(locatorNameForNLFFILterFieldValidation));
        /* Checking Area */
        if (elements.isDisplayed()) {
            String validationMessage = elements.getText();
            LOGGER.info(validationFor + " VALIDATION MESSAGE : " + validationMessage);
            LOGGER.info(validationFor + " VALIDATION MESSAGE DISPLAYED: Passed ");
            isValidationMessageDisplay = true;
            } else {
            if (!elements.getText().equals("null")) {
                LOGGER.info(validationFor + " WEB ELEMENT NOT DISPLAY: but try to get text : " + elements.getText());
            }
            LOGGER.info(validationFor + " VALIDATION MESSAGE NOT DISPLAYED: Failed ");
            isValidationMessageDisplay = false;
        }
        return isValidationMessageDisplay;
    }
    public boolean isCutomTemplate() throws Exception {
        boolean isCutomTemplate = false;
        // DO YOUR WORK HERE
        ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
        Object YAMLObjectValue = excelInputDataObject.getYAMLData();
        Map channelMap = (Map) YAMLObjectValue;
        if (channelMap.containsKey("configuration_file")) {
            isCutomTemplate = true;
        }
        return isCutomTemplate;
    }
    public boolean isNeedToGetValueFromExcelEvenIfNotCutomTemplate(String overrideKey) throws Exception {
        boolean isNeedToGetValueFromExcelEvenIfCutomTemplate = false;
        String overrideExcelValue = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount),
        overrideKey);
        if (overrideExcelValue != null) {
            if (overrideExcelValue.equals("Y")) {
                isNeedToGetValueFromExcelEvenIfCutomTemplate = true;
            }
        }
        return isNeedToGetValueFromExcelEvenIfCutomTemplate;
    }
    public boolean isMandatoryAvailable() throws Exception {
        boolean isMandatoryAvailable = false;
        if (getFilterMapFromYAML() != null) {
            if (getFilterMapFromYAML().containsKey("required")) {
                String isRequired = (String) getFilterMapFromYAML().get("required");
                if (isRequired.equals("true")) {
                    isMandatoryAvailable = true;
                }
            }
        }
        return isMandatoryAvailable;
    }
    public boolean isMinimumAvailable() throws Exception {
        if (getFilterMapFromYAML() != null) {
            return getFilterMapFromYAML().containsKey("minAmount");
        }
        return false;
    }
    public boolean isMaximumAvailable() throws Exception {
        boolean isMaximumAvailable = false;
        if (getFilterMapFromYAML() != null) {
            if (getFilterMapFromYAML().containsKey("maxAmount")) {
                isMaximumAvailable = true;
            }
        }
        return isMaximumAvailable;
    }
    /*
    * Data Provider Methods
    */
    public WebDriver getWebDriver() throws Exception {
        ExcelInputData excelInput = ExcelInputData.getInstance();
        WebDriver webDriver = excelInput.getWebDriver();
        return webDriver;
    }
    public WebElement getFilterWebElement() throws Exception {
        GetWebElement obj = new GetWebElement();
        String Locator = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount),
        "Web Element Locator");
        String LocatorName = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount), "Web Element");
        WebElement filterWebElement = obj.getWebElemntFromWebElemntType(getWebDriver(), Locator, LocatorName);
        return filterWebElement;
    }
    public String getCurrentMethodName(int filterMethodCount) throws Exception {
        ArrayList<HashMap<String, ExcelFilterInputData>> excelInputFilterArray = ExcelInputData.getInstance()
        .getExcelFilterInputData();
        HashMap<String, ExcelFilterInputData> excelInputFilterMap = excelInputFilterArray.get(filterMethodCount);
        String currentMethodName = (String) excelInputFilterMap.keySet().toArray()[filterMethodCount];
        return currentMethodName;
    }
    public int getTotalFilterMethodCount() throws Exception {
        ArrayList<HashMap<String, ExcelFilterInputData>> excelInputFilterArray = ExcelInputData.getInstance()
        .getExcelFilterInputData();
        int filterValidationCount = excelInputFilterArray.size();
        return filterValidationCount;
    }
    public String getCurrentCalculationInputForCurrentFilter(String currentMethodName, int forCalculationInput)
    throws Exception {
        ExcelCalculationInputData excelCalculationInputData = ExcelInputData.getInstance()
        .getExcelCalculationInputData();
        HashMap<String, ArrayList<String>> calculationResultArray = excelCalculationInputData.getMethodInput();
        ArrayList<String> singleKeyResult = calculationResultArray.get(currentMethodName);
        String calcInput = singleKeyResult.get(forCalculationInput);
        return calcInput;
    }
    public int getTotalCalculationCountForSingleFilter(String currentMethodName) throws Exception {
        ExcelCalculationInputData excelCalculationInputData = ExcelInputData.getInstance()
        .getExcelCalculationInputData();
        HashMap<String, ArrayList<String>> calculationResultArray = excelCalculationInputData.getMethodInput();
        ArrayList<String> singleKeyResult = calculationResultArray.get(currentMethodName);
        int totalFilterCalculationCount = singleKeyResult.size();
        return totalFilterCalculationCount;
    }
    // Get YAML Value Methods
    public Map getFilterMapFromYAML() throws Exception {
        Map FilterComponentMap = null;
        ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
        Object YAMLObjectValue = excelInputDataObject.getYAMLData();
        Map objectMap = (Map) YAMLObjectValue;
        Object FilterObject = objectMap.get("filter");
        Map FilterMap = (Map) FilterObject;
        if (!FilterMap.containsKey("custom_template")) {
            Object ValidationsObject = FilterMap.get("validations");
            Map ValidationsMap = (Map) ValidationsObject;
            Object RulesObject = ValidationsMap.get("rules");
            Map RulesMap = (Map) RulesObject;
            String KEYInYAML = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount),
            "Filter Key In YAML");
            Object FilterComponentObject = RulesMap.get(KEYInYAML);
            FilterComponentMap = (Map) FilterComponentObject;
        }
        return FilterComponentMap;
    }
    public Map getTableFieldsMapFromYAML() throws Exception {
        Map FilterComponentMap = null;
        ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
        Object YAMLObjectValue = excelInputDataObject.getYAMLData();
        Map objectMap = (Map) YAMLObjectValue;
        Object FilterObject = objectMap.get("table_fields");
        Map FilterMap = (Map) FilterObject;
        if (!FilterMap.containsKey("custom_template")) {
            Object ColumbObject = FilterMap.get("columns");
            Map ColumnMap = (Map) ColumbObject;
            Object FieldMapObject = ColumnMap.get(getCurrentMethodName(this.filterMethodInvocationCount));
            FilterComponentMap = (Map) FieldMapObject;
        }
        return FilterComponentMap;
    }
    public String getMandatoryElementNameFromYAML() throws Exception {
        String mandatoryElementNameFromYAML = "";
        if (getFilterMapFromYAML().containsKey("required")) {
            mandatoryElementNameFromYAML = (String) getFilterMapFromYAML().get("required");
        }
        return mandatoryElementNameFromYAML;
    }
    public String getMinimumValueFromYAML() throws Exception {
        String minimumValueFromYAML = "";
        if (getFilterMapFromYAML().containsKey("minAmount")) {
            minimumValueFromYAML = (String) getFilterMapFromYAML().get("minAmount");
        }
        return minimumValueFromYAML;
    }
    public String getMaximumValueFromYAML() throws Exception {
        String minimumValueFromYAML = "";
        if (getFilterMapFromYAML().containsKey("maxAmount")) {
            minimumValueFromYAML = (String) getFilterMapFromYAML().get("maxAmount");
        }
        return minimumValueFromYAML;
    }
    public String getLocatorNameFromYAML() throws Exception {
        String locatorName = "";
        if (getFilterMapFromYAML().containsKey("span_class")) {
            locatorName = (String) getTableFieldsMapFromYAML().get("span_class");
        }
        return locatorName;
    }
    // Get Excel Value Methods
    public String getValueFromExcel(String methodNameKey, String valueKey) throws Exception {
        String valueFromExcel = this.excelInputInstance
        .get_A_Value_Using_Key_Of_Filters_Method(methodNameKey, valueKey);
        return valueFromExcel;
    }
    // Get Required Input From Excel or YAML
    public String getMinimumValue() throws Exception {
        String requiredValue = "";
        if (!isCutomTemplate()) {
            if (isNeedToGetValueFromExcelEvenIfNotCutomTemplate("Override Yaml Value")) {
                // Get any values here if you need any values from excel even if
                // it is a not a custom template
                requiredValue = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount),
                "Minimum Value");
                } else {
                // get This value from YAML
                requiredValue = getMinimumValueFromYAML();
            }
            } else {
            // Get Valued from EXCEL
            requiredValue = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount), "Minimum Value");
        }
        return requiredValue;
    }
    public String getMaximumValue() throws Exception {
        String requiredValue = "";
        if (!isCutomTemplate()) {
            if (isNeedToGetValueFromExcelEvenIfNotCutomTemplate("Override Yaml Value")) {
                // Get any values here if you need any values from excel even if
                // it is a not a custom template
                requiredValue = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount),
                "Maximum Value");
                } else {
                // get This value from YAML
                requiredValue = getMaximumValueFromYAML();
            }
            } else {
            // Get Valued from EXCEL
            requiredValue = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount), "Maximum Value");
        }
        return requiredValue;
    }
    public String getLocator() throws Exception {
        String requiredValue = "";
        // Here input control is not given in YAML, so we directly get it from
        // excel
        requiredValue = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount), "Input Control");
        return requiredValue;
    }
    public String getLocatorName() throws Exception {
        String requiredValue = "";
        if (!isCutomTemplate()) {
            if (isNeedToGetValueFromExcelEvenIfNotCutomTemplate("Override Yaml Value")) {
                // Get any values here if you need any values from excel even if
                // it is a not a custom template
                requiredValue = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount), "Web Element");
                } else {
                // get This value from YAML
                requiredValue = getLocatorNameFromYAML();
            }
            } else {
            // Get Valued from EXCEL
            requiredValue = getValueFromExcel(getCurrentMethodName(this.filterMethodInvocationCount), "Web Element");
        }
        return requiredValue;
    }
}
package com.moneysmart.channel.test;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.annotations.*;

import com.common.utility.WebserviceRequest;
import com.common.utility.WebserviceRequestListener;

import com.model.Constants.TestResult;
import static com.model.Constants.MyStore;
import com.model.ExcelInputData;
import com.model.GetExcelInput;
import com.model.Constants.ExcelMethodInput;

import java.util.logging.Level;
import java.util.logging.Logger;
public class TDetails implements WebserviceRequestListener {
    private static final Logger LOGGER = Logger.getLogger(TDetails.class.getName());
    private WebserviceRequestListener webserviceListener;
    private String Exception_String = "EXCEPTION";
    /*
    * Data Provider Methods
    */
    String expectedResultExcel = "";
    String testDescriptionExcel = "";
    String actualResultExcel = "";
    String executeOrNotExcel = "";
    String expecedResultFromExcel = "";
    String actualFilterFailureResult = "";
    String actualResultFromExcel = "";
    boolean isAllDetailsPassed = true;
    @Test
    public void ZDetailtest(ITestContext testContext) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        String TestMethodName = "ZDetailtest";
        String TestMethodNameReport = "Detail Validation";
        WebElement element = null;
        try {
            ExcelInputData excelinputobj = ExcelInputData.getInstance();
            Object productobjYAML = excelinputobj.getYAMLData();
            Map productmap = (Map) productobjYAML;
            Object tablefields = productmap.get("detail_fields");
            GetExcelInput getInput = new GetExcelInput();
            List<Map> tablefieldarray = null;
            int webElementCoutn = 0;
            String cururls = getWebDriver().getCurrentUrl();
            if (cururls.contains("travel-insurance")) {
                String TravelInconcience = "Travel Inconvenience";
                String MedicalCoverage = "Medical Coverage";
                String PersonalProtection = "Personal Protection";
                String Others = "Others";
                String traveldelay = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.TravelInconvenience_MethodNameKey, ExcelMethodInput.TravelDelay_nodeKey);
                String TravelCancelation = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.TravelInconvenience_MethodNameKey,
                ExcelMethodInput.TravelCancelationnode_Key);
                String DelayedBaggage = getInput
                .get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.TravelInconvenience_MethodNameKey,
                ExcelMethodInput.DelayedBaggage_nodeKey);
                String TripCurtailment = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.TravelInconvenience_MethodNameKey,
                ExcelMethodInput.DelayedBaggage_nodeKey);
                String Missedflightconnection = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.TravelInconvenience_MethodNameKey,
                ExcelMethodInput.Missedflightconnection_nodeKey);
                String LossDamageofBaggage = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.TravelInconvenience_MethodNameKey,
                ExcelMethodInput.LossDamageofBaggage_nodeKey);
                String LossofTravelDocuments = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.TravelInconvenience_MethodNameKey,
                ExcelMethodInput.LossofTravelDocuments_nodeKey);
                String overseasexpence = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.MedicalCoverage_MethodNameKey,
                ExcelMethodInput.OverseasMedicalExpenses_nodeKey);
                String dailymedical = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.MedicalCoverage_MethodNameKey,
                ExcelMethodInput.DailyHospitalAllowance_nodeKey);
                String PostTripMedicalExpenses = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.MedicalCoverage_MethodNameKey,
                ExcelMethodInput.PostTripMedicalExpenses_nodeKey);
                String deathdisability = getInput.get_A_Value_Using_Key_Of_A_Method(
                ExcelMethodInput.PersonalProtection_MethodNameKey,
                ExcelMethodInput.DeathDisabillity_nodeKey);
                // Map One
                Map<String, String> mapTravelIncon = new HashMap<>();
                mapTravelIncon.put(ExcelMethodInput.TravelDelay_nodeKey, traveldelay);
                mapTravelIncon.put(ExcelMethodInput.TravelCancelationnode_Key, TravelCancelation);
                mapTravelIncon.put(ExcelMethodInput.DelayedBaggage_nodeKey, DelayedBaggage);
                mapTravelIncon.put(ExcelMethodInput.DelayedBaggage_nodeKey, TripCurtailment);
                mapTravelIncon.put(ExcelMethodInput.Missedflightconnection_nodeKey, Missedflightconnection);
                mapTravelIncon.put(ExcelMethodInput.LossDamageofBaggage_nodeKey, LossDamageofBaggage);
                mapTravelIncon.put(ExcelMethodInput.LossofTravelDocuments_nodeKey, LossofTravelDocuments);
                Map<String, Map> mapOne = new HashMap<String, Map>();
                mapOne.put(TravelInconcience, mapTravelIncon);
                // Map Two
                Map<String, String> mapMedicalCo = new HashMap<>();
                mapMedicalCo.put(ExcelMethodInput.OverseasMedicalExpenses_nodeKey, overseasexpence);
                mapMedicalCo.put(ExcelMethodInput.DailyHospitalAllowance_nodeKey, dailymedical);
                mapMedicalCo.put(ExcelMethodInput.PostTripMedicalExpenses_nodeKey, PostTripMedicalExpenses);
                Map<String, Map> mapTwo = new HashMap<String, Map>();
                mapTwo.put(MedicalCoverage, mapMedicalCo);
                // Map Three
                Map<String, String> mapPersonalPro = new HashMap<>();
                mapPersonalPro.put(ExcelMethodInput.DeathDisabillity_nodeKey, deathdisability);
                Map<String, Map> mapThree = new HashMap<String, Map>();
                mapThree.put(PersonalProtection, mapPersonalPro);
                tablefieldarray = new ArrayList<Map>();
                tablefieldarray.add(mapOne);
                tablefieldarray.add(mapTwo);
                tablefieldarray.add(mapThree);
                } else {
                tablefieldarray = (ArrayList<Map>) tablefields;
            }
            for (Map singleMapInTable : tablefieldarray) {
                LOGGER.info("SECTION maps: " + singleMapInTable.toString());
                for (Object sectionMapKey : singleMapInTable.keySet()) {
                    LOGGER.info("SECTION: " + sectionMapKey.toString());
                    //
                    Map<String, String> SECTION = new HashMap<>();
                    String parentelement = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                    "ParentElement");
                    String childelement = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                    "ClildElement");
                    String keyelement = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                    "KeyElement");
                    String valueelement = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                    "ValueElement");
                    String testDescriptionExcels = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                    "Test Description");
                    String expecedResultFromExcels = getInput.get_A_Value_Using_Key_Of_A_Method(
                    sectionMapKey.toString(), "Expected Test Result");
                    String executeOrNotExcels = getInput.get_A_Value_Using_Key_Of_A_Method(sectionMapKey.toString(),
                    "Execute");
                    executeOrNotExcel = executeOrNotExcels;
                    if (expecedResultFromExcel.length() > 0) {
                        expecedResultFromExcel = expecedResultFromExcel + " , " + expecedResultFromExcels;
                        } else {
                        expecedResultFromExcel = expecedResultFromExcels;
                    }
                    if (testDescriptionExcel.length() > 0) {
                        testDescriptionExcel = testDescriptionExcel + " , " + testDescriptionExcels;
                        } else {
                        testDescriptionExcel = testDescriptionExcels;
                    }
                    List<WebElement> contentList = null;
                    List<String> contentListFromWebForKey = null;
                    List<String> contentListFromWebForValue = null;
                    int contentListSize = 0;
                    if (cururls.contains("travel-insurance")) {
                        String traveldelaytitleweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.TravelInconvenience_MethodNameKey,
                        ExcelMethodInput.TravelDelay_webTitleKey);
                        String delayedbaggagetitleweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.TravelInconvenience_MethodNameKey,
                        ExcelMethodInput.DelayedBaggage_TitleKey);
                        String TravelCancelationtitleweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.TravelInconvenience_MethodNameKey,
                        ExcelMethodInput.TravelCancelation_TitleKey);
                        String overseasexpencetitleweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.MedicalCoverage_MethodNameKey,
                        ExcelMethodInput.OverseasMedicalExpenses_TitleKey);
                        String dailymedicaltitleweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.MedicalCoverage_MethodNameKey,
                        ExcelMethodInput.DailyHospitalAllowance_TitleKey);
                        String deathdisabilitytitleweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.PersonalProtection_MethodNameKey,
                        ExcelMethodInput.DeathDisabillity_TitleKey);
                        contentListFromWebForKey = new ArrayList<String>();
                        contentListFromWebForKey.add(traveldelaytitleweb);
                        contentListFromWebForKey.add(delayedbaggagetitleweb);
                        contentListFromWebForKey.add(TravelCancelationtitleweb);
                        contentListFromWebForKey.add(overseasexpencetitleweb);
                        contentListFromWebForKey.add(dailymedicaltitleweb);
                        contentListFromWebForKey.add(deathdisabilitytitleweb);
                        String traveldelayweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.TravelInconvenience_MethodNameKey,
                        ExcelMethodInput.TravelDelay_webKey);
                        String delayedbaggageweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.TravelInconvenience_MethodNameKey,
                        ExcelMethodInput.DelayedBaggage_webKey);
                        String TravelCancelationweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.TravelInconvenience_MethodNameKey,
                        ExcelMethodInput.TravelCancelation_webKey);
                        String overseasexpenceweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.MedicalCoverage_MethodNameKey,
                        ExcelMethodInput.OverseasMedicalExpenses_webKey);
                        String dailymedicalweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.MedicalCoverage_MethodNameKey,
                        ExcelMethodInput.DailyHospitalAllowance_webKey);
                        String deathdisabilityweb = getInput.get_A_Value_Using_Key_Of_A_Method(
                        ExcelMethodInput.PersonalProtection_MethodNameKey,
                        ExcelMethodInput.DeathDisabillity_webKey);
                        contentListFromWebForValue = new ArrayList<String>();
                        contentListFromWebForValue.add(traveldelayweb);
                        contentListFromWebForValue.add(delayedbaggageweb);
                        contentListFromWebForValue.add(TravelCancelationweb);
                        contentListFromWebForValue.add(overseasexpenceweb);
                        contentListFromWebForValue.add(dailymedicalweb);
                        contentListFromWebForValue.add(deathdisabilityweb);
                        contentListSize = contentListFromWebForKey.size();
                        LOGGER.info("contentList size for TI: " + contentListFromWebForKey.size());
                        } else {
                        WebElement webElement = getWebDriver().findElement(By.xpath(parentelement));
                        contentList = webElement.findElements(By.className(childelement));
                        contentListSize = contentList.size();
                    }
                    for (int contentValueCount = 0; contentValueCount < contentListSize; contentValueCount++) {
                        String productNameWeb = "";
                        String producttitWeb = "";
                        if (cururls.contains("travel-insurance")) {
                            String webXpathStringToGetWebElementKey = contentListFromWebForKey.get(contentValueCount);
                            String webXpathStringToGetWebElementValue = contentListFromWebForValue
                            .get(contentValueCount);
                            LOGGER.info("TI webXpathStringToGetWebElementKey: " + webXpathStringToGetWebElementKey);
                            producttitWeb = getWebDriver().findElement(By.xpath(webXpathStringToGetWebElementKey))
                            .getText();
                            productNameWeb = getWebDriver().findElement(By.xpath(webXpathStringToGetWebElementValue))
                            .getText();
                            LOGGER.info("TI channel: " + productNameWeb);
                            LOGGER.info("TI channel: " + producttitWeb);
                            } else {
                            WebElement singleSectionWebElement = contentList.get(contentValueCount);
                            productNameWeb = singleSectionWebElement.findElement(By.className(valueelement)).getText();
                            producttitWeb = singleSectionWebElement.findElement(By.className(keyelement)).getText();
                            LOGGER.info("Other channel: ");
                        }
                        LOGGER.info("producttitWeb: " + producttitWeb);
                        LOGGER.info("productNameWeb: " + productNameWeb);
                        SECTION.put(producttitWeb, productNameWeb);
                    }
                    LOGGER.info("SECTION From web: " + SECTION);
                    Map fields = (Map) singleMapInTable.get(sectionMapKey);
                    String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(
                    ExcelMethodInput.Openwebpage_MethodNameKey,
                    ExcelMethodInput.Openwebpage_ChaneelNameKey);
                    String channelUURL = getInput.get_A_Value_Using_Key_Of_A_Method(
                    ExcelMethodInput.Openwebpage_MethodNameKey,
                    ExcelMethodInput.Openwebpage_ChanellURlKey);
                    for (Object filedKey : fields.keySet()) {
                        LOGGER.info("filedKey" + filedKey.toString());
                        if (filedKey.toString().equals("fields") || cururls.contains("travel-insurance")) {
                            LOGGER.info("FIELD KEY : " + filedKey.toString());
                            LOGGER.info("FIELD KEY VALUE: " + fields.get(filedKey));
                            if (fields.get(filedKey) instanceof Map || cururls.contains("travel-insurance")) {
                                Map field_map = fields;
                                if (cururls.contains("travel-insurance")) {
                                   
                                    } else {
                                    if (filedKey.toString().equals("fields")) {
                                        LOGGER.info("filedKey" + filedKey.toString());
                                        LOGGER.info("filedKeyValue " + fields.get(filedKey));
                                        if (fields.get(filedKey) instanceof Map)
                                        field_map = (Map) fields.get(filedKey);
                                        
                                        } else {
                                        LOGGER.info("PLEASE CHECK... whether field key is not available in ");
                                    }
                                }
                                
                                for (Object key : field_map.keySet()) {
                                    String str = "";
                                    if (channelName.equals("auto-loans")) {
                                        LOGGER.info("Key_IF " + key.toString());
                                        LOGGER.info("cnkey.toString()_If " + sectionMapKey.toString());
                                        String keyValFromYAML = key.toString();
                                        if (keyValFromYAML.equals("features.admin_fee")) {
                                            keyValFromYAML = "Admin Fee";
                                            } else if (keyValFromYAML.equals("features.other_fee")) {
                                            keyValFromYAML = "Other Fees";
                                            } else if (keyValFromYAML.equals("features.late_fee")) {
                                            keyValFromYAML = "Late Fee";
                                            } else if (keyValFromYAML.equals("features.early_settlement_fee")) {
                                            keyValFromYAML = "Early Settlement Fee";
                                        }
                                        else if (keyValFromYAML.equals("requirements.citizenship")) {
                                            keyValFromYAML = "Citizenship Status";
                                            } else if (keyValFromYAML.equals("requirements.age_min")) {
                                            keyValFromYAML = "Minimum Age";
                                            } else if (keyValFromYAML.equals("requirements.age_max")) {
                                            keyValFromYAML = "Maximum Age";
                                            } else if (keyValFromYAML.equals("Other Requirements")) {
                                            keyValFromYAML = "Other Requirements";
                                            } else if (keyValFromYAML.equals("requirements.employment")) {
                                            keyValFromYAML = "Employment Status";
                                            } else if (keyValFromYAML.equals("requirements.doc_ktp")) {
                                            keyValFromYAML = "KTP/ID Required?";
                                            } else if (keyValFromYAML.equals("requirements.doc_pay_slip")) {
                                            keyValFromYAML = "Salary Slip Required?";
                                            } else if (keyValFromYAML.equals("requirements.coverage_area")) {
                                            keyValFromYAML = "Coverage Area?";
                                        }
                                        if (!keyValFromYAML.equals("Coverage Area?")) {
                                            str = getAutoLoanValue(sectionMapKey.toString(), keyValFromYAML);
                                            LOGGER.info("str for Auto-loan = not use_common " + str);
                                            compareWebAndJson(filedKey.toString(), SECTION.get(keyValFromYAML), str);
                                        }
                                    } else if (channelName.equals("personal-loans")
                                    && channelUURL.equals("https://www.duitpintar.com/en_sg/unsecured-loans")) {
                                        LOGGER.info("Key_IF " + key.toString());
                                        LOGGER.info("cnkey.toString()_If " + sectionMapKey.toString());
                                        String keyValFromYAML = key.toString();
                                        if (keyValFromYAML.equals("features.late_payment_fee")) {
                                            keyValFromYAML = "Late Payment Fee";
                                            } else if (keyValFromYAML.equals("features.other_fee")) {
                                            keyValFromYAML = "Other Fees";
                                        }
                                        else if (keyValFromYAML.equals("requirements.citizenship")) {
                                            keyValFromYAML = "Citizenship Status";
                                            } else if (keyValFromYAML.equals("requirements.age")) {
                                            keyValFromYAML = "Age Requirement";
                                            } else if (keyValFromYAML.equals("requirements.employment")) {
                                            keyValFromYAML = "Employment Status";
                                            } else if (keyValFromYAML.equals("requirements.doc_ktp")) {
                                            keyValFromYAML = "KTP/ID Required?";
                                            } else if (keyValFromYAML.equals("requirements.doc_pay_slip")) {
                                            keyValFromYAML = "Salary Slip Required?";
                                            } else if (keyValFromYAML.equals("requirements.others_requirement")) {
                                            keyValFromYAML = "Others Requirement";
                                            } else if (keyValFromYAML.equals("requirements.coverage_area")) {
                                            keyValFromYAML = "Coverage Area?";
                                        }
                                        else if (keyValFromYAML.equals("features.admin_fee")) {
                                            keyValFromYAML = "Admin Fee";
                                            } else if (keyValFromYAML.equals("features.early_settlement_fee")) {
                                            keyValFromYAML = "Early Settlement Fee";
                                        }
                                        else if (keyValFromYAML.equals("specifications.min_loan_amount_txt")) {
                                            keyValFromYAML = "Minimum Loan Amount";
                                            } else if (keyValFromYAML.equals("specifications.max_loan_amount_txt")) {
                                            keyValFromYAML = "Maximum Loan Amount";
                                            } else if (keyValFromYAML.equals("specifications.max_loan_tenure_txt")) {
                                            keyValFromYAML = "Maximum Loan Tenure";
                                            } else if (keyValFromYAML.equals("Interest Rate")) {
                                            keyValFromYAML = "Interest Rate";
                                            } else if (keyValFromYAML.equals("features.approval_duration")) {
                                            keyValFromYAML = "Approval Duration";
                                            } else if (keyValFromYAML.equals("specifications.min_annual_income_txt")) {
                                            keyValFromYAML = "Minimum Monthly Income";
                                        }
                                        LOGGER.info(" Unsecured-loan = not use_common ");
                                        if (!keyValFromYAML.equals("Coverage Area?")) {
                                            str = getUnSecuredLoanValue(sectionMapKey.toString(), keyValFromYAML);
                                            LOGGER.info("str for Unsecured-loan = not use_common " + str);
                                            compareWebAndJson(filedKey.toString(), SECTION.get(keyValFromYAML), str);
                                        }
                                        } else {
                                        str = fetchValueFromJSONFor(removeBraces((String) field_map.get(key)));
                                        LOGGER.info(" strstrstrstr: " + str);
                                        if (SECTION.containsKey(key)) {
                                            LOGGER.info(" If VAl From Api str: " + str);
                                            compareWebAndJson(filedKey.toString(), SECTION.get(key), str);
                                            } else if (cururls.contains("travel-insurance")) {
                                            LOGGER.info(" If VAl From Api str: " + str);
                                            LOGGER.info(" If VAl From Api str: " + key);
                                            LOGGER.info(" If VAl From Api SECTION " + SECTION);
                                            LOGGER.info(" If VAl From Api SECTION.get(key): " + SECTION.get(key));
                                            compareWebAndJson(key.toString(), SECTION.get(key), str);
                                        }
                                    }
                                }
                                } else {
                                ArrayList field_map = (ArrayList) fields.get(filedKey);
                                if ((field_map.get(0)).equals("use_common")) {
                                    Object commonfields = productmap.get("common");
                                    Map commonfieldarray = (Map) commonfields;
                                    for (Object commonkey : commonfieldarray.keySet()) {
                                        LOGGER.info("commonkey" + commonkey);
                                        for (Object common : ((Map) commonfieldarray.get(commonkey)).keySet()) {
                                            LOGGER.info("common" + common);
                                            if (common.toString().equals("label")) {
                                                LOGGER.info("commonkey2 "
                                                + (((Map) commonfieldarray.get(commonkey))).get(common));
                                                for (Object fieldKey : ((Map) (((Map) commonfieldarray.get(commonkey)))
                                                .get(common)).keySet()) {
                                                    LOGGER.info("commonkey3 "
                                                    + (String) ((Map) (((Map) commonfieldarray.get(commonkey)))
                                                    .get(common)).get(fieldKey));
                                                    if (((String) ((Map) (((Map) commonfieldarray.get(commonkey)))
                                                .get(common)).get(fieldKey)).contains("} {")) {
                                                        String[] string = ((String) ((Map) (((Map) commonfieldarray
                                                        .get(commonkey))).get(common)).get(fieldKey))
                                                        .split("\\} \\{");
                                                        LOGGER.info("commonkey4 " + string[0].toString()
                                                        + string[1].toString());
                                                        String str = "";
                                                        if (channelName.equals("auto-loans")) {
                                                            LOGGER.info("fieldKey " + fieldKey.toString());
                                                            LOGGER.info("SECTION.get(fieldKey) "
                                                            + SECTION.get(fieldKey));
                                                            LOGGER.info("commonkey.toString() " + commonkey.toString());
                                                            String spliteMaxTenure = SECTION.get(fieldKey).split(" ")[0]
                                                            .trim();
                                                            str = fetchValueFromJSONFor(removeBraces(string[0]));
                                                            if (fieldKey.toString().trim().contains("Max. Tenure")) {
                                                                compareWebAndJson(commonkey.toString(),
                                                                spliteMaxTenure, str);
                                                                } else {
                                                                compareWebAndJson(commonkey.toString(),
                                                                SECTION.get(fieldKey), str);
                                                            }
                                                            } else {
                                                            str = fetchValueFromJSONFor(removeBraces(string[0]))
                                                            + fetchValueFromJSONFor(removeBraces(string[1]));
                                                            compareWebAndJson(commonkey.toString(),
                                                            SECTION.get(fieldKey), str);
                                                        }
                                                        } else {
                                                        if (channelName.equals("auto-loans")) {
                                                            LOGGER.info("fieldKey not } { " + fieldKey.toString());
                                                            LOGGER.info("SECTION.get(fieldKey) } {"
                                                            + SECTION.get(fieldKey));
                                                            LOGGER.info("commonkey.toString() } {"
                                                            + commonkey.toString());
                                                            String spliteMaxTenure = SECTION.get(fieldKey).split(" ")[0]
                                                            .trim();
                                                            String str = fetchValueFromJSONFor(removeBraces((String) ((Map) (((Map) commonfieldarray
                                                            .get(commonkey))).get(common)).get(fieldKey)));
                                                            if (fieldKey.toString().trim().contains("Max. Tenure")) {
                                                                if (SECTION.containsKey(fieldKey)) {
                                                                    compareWebAndJson(commonkey.toString(),
                                                                    spliteMaxTenure, str);
                                                                }
                                                                } else {
                                                                if (SECTION.containsKey(fieldKey)) {
                                                                    compareWebAndJson(commonkey.toString(),
                                                                    SECTION.get(fieldKey), str);
                                                                }
                                                            }
                                                            } else {
                                                            String str = fetchValueFromJSONFor(removeBraces((String) ((Map) (((Map) commonfieldarray
                                                            .get(commonkey))).get(common)).get(fieldKey)));
                                                            if (SECTION.containsKey(fieldKey)) {
                                                                compareWebAndJson(commonkey.toString(),
                                                                SECTION.get(fieldKey), str);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            webElementCoutn++;
            resultMap.put(TestResult.R_METHOD_NAME, TestMethodNameReport);
            resultMap.put(TestResult.R_EXPECTED_RESULT, "The Values in API and Website Should be Matched");
            resultMap.put(TestResult.R_ACTUAL_RESULT, "Detail Validation Passed");
            resultMap.put(TestResult.R_DESRIPTION, "Shoud Validate the Values in Api and Web");
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, "Y");
            resultMap.put(TestResult.R_IS_SUCCESS, isAllDetailsPassed);
            resultMap.put(TestResult.R_MESSAGE, actualFilterFailureResult);
            testContext.setAttribute(TestMethodName, resultMap);
            if (!isAllDetailsPassed) {
            }
            } catch (Exception e) {
            resultMap.put(TestResult.R_METHOD_NAME, TestMethodNameReport);
            resultMap.put(TestResult.R_IS_SUCCESS, false);
            resultMap.put(TestResult.R_MESSAGE, "Detail Validation Failed");
            resultMap.put(TestResult.R_EXCEPTION_ERROR_MESSAGE, "Detail Validation failed");
            resultMap.put(TestResult.R_METHOD_EXECUTE_OR_NOT, "Y");
            resultMap.put(TestResult.R_EXPECTED_RESULT, "The Values in API and Website Should be Matched");
            resultMap.put(TestResult.R_DESRIPTION, "Shoud Validate the Values in Api and Web");
            resultMap.put(TestResult.R_ACTUAL_RESULT, "Detail Validation Passed");
            testContext.setAttribute(TestMethodName, resultMap);
            LOGGER.info("Failure method name = " + resultMap.get(TestResult.R_METHOD_NAME));
            LOGGER.log(Level.SEVERE, Exception_String, e);
           
        }
    }
 
    private String getUnSecuredLoanValue(String commonkey, String fieldKey) throws Exception {
        String str = "";
        if (commonkey.toString().contains("Penalties & Late Fees")) {
            if (fieldKey.equals("Late Payment Fee")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.late_payment_fee"));
                } else if (fieldKey.equals("Other Fees")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.cancellation_fee"));
            }
        }
        else if (commonkey.toString().contains("Eligibility")) {
            if (fieldKey.equals("Citizenship Status")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.citizenship"));
                } else if (fieldKey.equals("Employment Status")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.employment"));
                } else if (fieldKey.equals("Age Requirement")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.age"));
                } else if (fieldKey.equals("KTP/ID Required?")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.doc_ktp"));
                } else if (fieldKey.equals("Salary Slip Required?")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_requirements.doc_pay_slip"));
                } else if (fieldKey.equals("Others Requirement")) {
                str = fetchValueFromJSONFor(removeBraces("product.requirements.others_requirement"));
            }
        }
        else if (commonkey.toString().contains("One-time Fees & Charges")) {
            if (fieldKey.equals("Admin Fee")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.admin_fee"));
                } else if (fieldKey.equals("Early Settlement Fee")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.early_settlement_fee"));
            }
        }
        else if (commonkey.toString().contains("Key Product Features")) {
            if (fieldKey.equals("Minimum Loan Amount")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_specifications.array_value.min_loan_amount_txt"));
                } else if (fieldKey.equals("Maximum Loan Amount")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_specifications.array_value.max_loan_amount_txt"));
                } else if (fieldKey.equals("Maximum Loan Tenure")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_specifications.array_value.max_loan_tenure_txt"));
                } else if (fieldKey.equals("Interest Rate")) {
                str = fetchValueFromJSONFor(removeBraces("product.calculation_result.apr"));
                } else if (fieldKey.equals("Approval Duration")) {
                str = fetchValueFromJSONFor(removeBraces("product.reformat_features.approval_duration.value"));
                } else if (fieldKey.equals("Minimum Monthly Income")) {
                str = fetchValueFromJSONFor(removeBraces("product.requirements.income_min"));
            }
        }
        return str.trim();
    }
    private String getAutoLoanValue(String commonkey, String fieldKey) throws Exception {
        
        String str = "";
        if (commonkey.toString().contains("Penalties & Late Fees")) {
            if (fieldKey.equals("Admin Fee")) {
                str = fetchValueFromJSONFor("product.features.admin_fee");
                LOGGER.info("Admin Fee " + str);
            }
            else if (fieldKey.equals("Other Fees")) {
                str = fetchValueFromJSONFor("product.features.other_fee");
                LOGGER.info("Other Fees " + str);
                } else if (fieldKey.equals("Late Fee")) {
                str = fetchValueFromJSONFor("product.features.late_fee");
                LOGGER.info("Late Fee " + str);
                } else if (fieldKey.equals("Early Settlement Fee")) {
                str = fetchValueFromJSONFor("product.features.early_settlement_fee");
                LOGGER.info("Early Settlement Fee " + str);
            }
        }
        else if (commonkey.toString().contains("Eligibility")) {
            if (fieldKey.equals("Citizenship Status")) {
                str = fetchValueFromJSONFor("product.requirements.citizen_only");
                LOGGER.info("citizen_only " + str);
                } else if (fieldKey.equals("Minimum Age")) {
                str = fetchValueFromJSONFor("product.requirements.age_min");
                LOGGER.info("age_min " + str);
                } else if (fieldKey.equals("Maximum Age")) {
                str = fetchValueFromJSONFor("product.requirements.age_max");
                LOGGER.info("age_max " + str);
                } else if (fieldKey.equals("Other Requirements")) {
                str = fetchValueFromJSONFor("product.requirements.other_requirement");
                LOGGER.info("other_requirement " + str);
            }
            else if (fieldKey.equals("Employment Status")) {
                str = fetchValueFromJSONFor("product.requirements.employment.content");
                LOGGER.info("employement " + str);
                } else if (fieldKey.equals("KTP/ID Required?")) {
                str = fetchValueFromJSONFor("product.requirements.doc_ktp");
                LOGGER.info("doc_ktp " + str);
                } else if (fieldKey.equals("Salary Slip Required?")) {
                str = fetchValueFromJSONFor("product.reformat_requirements.doc_pay_slip.value");
                LOGGER.info("doc_pay " + str);
                } else if (fieldKey.equals("Coverage Area?")) {
                str = fetchValueFromJSONFor("requirements.coverage_area");
                LOGGER.info("doc_pay " + str);
            }
        }
        return str.trim();
    }
    private String fetchValueFromJSONFor(String keyFromYAML) throws Exception {
        
        String[] splittednode = keyFromYAML.split("\\.");
        LOGGER.info("keyFromYAML fetchValueFromJSONFor : " + keyFromYAML);
        String individualproductJson = Singleproductjson();
        JSONObject singleProduct = new JSONObject(individualproductJson);
        ExcelInputData excelInput = ExcelInputData.getInstance();
        Object value = singleProduct;
        for (int i = 0; i < (splittednode.length); i++) {
        }
        for (int i = 0; i < (splittednode.length - 1); i++) {
            LOGGER.info("splittednode:" + splittednode);
            LOGGER.info("splittednode length:" + splittednode.length);
            if (value instanceof JSONObject) {
                if (((JSONObject) value).has("premium")) {
                    Object premiumObject = (Object) ((JSONObject) value).get("premium");
                    if (premiumObject instanceof JSONObject) {
                        JSONObject premiumProduct = ((JSONObject) value).getJSONObject("premium");
                        value = premiumProduct.getJSONObject("sponsored");
                        } else {
                        value = ((JSONObject) value).get(splittednode[i]);
                    }
                    } else {
                    value = ((JSONObject) value).get(splittednode[i]);
                    if (value instanceof JSONObject) {
                        //
                        } else {
                        if (splittednode[i].equals("features") || splittednode[i].equals("requirements")) {
                            JSONArray temp = (JSONArray) value;
                            for (int j = 0; j < temp.length(); j++) {
                                if (((JSONObject) ((JSONArray) temp).getJSONObject(j)).getString("code").equals(
                                splittednode[i + 1])) {
                                    value = (JSONObject) ((JSONArray) temp).getJSONObject(j);
                                    if (((JSONObject) value).getString("value").equals("null")
                                    || ((JSONObject) value).getString("value").isEmpty()) {
                                        splittednode[i + 1] = "value_str";
                                        LOGGER.info("value_str 123456:" + splittednode[0] + splittednode[1]
                                        + splittednode[2]);
                                        } else {
                                        splittednode[i + 1] = "value";
                                        LOGGER.info("value 12345678:" + splittednode[0] + splittednode[1]
                                        + splittednode[2]);
                                    }
                                }
                            }
                            } else {
                            value = ((JSONObject) (((JSONArray) value).get(0))).get(splittednode[i]);
                        }
                    }
                }
                } else {
                LOGGER.info("feature or requirement:" + splittednode[i]);
                if (splittednode[i].equals("features") || splittednode[i].equals("requirements")) {
                    value = (JSONArray) ((JSONObject) (((JSONArray) value).get(0))).getJSONArray(splittednode[i]);
                    JSONArray temp = (JSONArray) value;
                    for (int j = 0; j < temp.length(); j++) {
                        if (((JSONObject) ((JSONArray) temp).getJSONObject(j)).getString("code").equals(
                        splittednode[i + 1])) {
                            value = (JSONObject) ((JSONArray) temp).getJSONObject(j);
                            if (((JSONObject) value).getString("value").equals("null")
                            || ((JSONObject) value).getString("value").isEmpty()) {
                                splittednode[i + 1] = "value_str";
                                LOGGER.info("value_str 123456:" + splittednode[0] + splittednode[1] + splittednode[2]);
                                } else {
                                splittednode[i + 1] = "value";
                                LOGGER.info("value 12345678:" + splittednode[0] + splittednode[1] + splittednode[2]);
                            }
                        }
                    }
                    } else {
                    value = ((JSONObject) (((JSONArray) value).get(0))).get(splittednode[i]);
                }
            }
        }
        String str = "";
        if (value instanceof JSONObject) {
            if (((JSONObject) value).get(splittednode[splittednode.length - 1]) != null) {
                String cururl = getWebDriver().getCurrentUrl();
                if (splittednode[splittednode.length - 1].toString().equals("max_loan_tenure")
                && cururl.contains("renovation")) {
                    String withoutCalc = ((JSONObject) value).get(splittednode[splittednode.length - 1]).toString();
                    Integer intcalVal = Integer.parseInt(withoutCalc);
                    int finalVal = intcalVal / 12;
                    str = String.valueOf(finalVal) + " years";
                   
                } else if (splittednode[splittednode.length - 1].toString().equals("max_loan_tenure")
                && cururl.contains("education")) {
                    String withoutCalc = ((JSONObject) value).get(splittednode[splittednode.length - 1]).toString();
                    Integer intcalVal = Integer.parseInt(withoutCalc);
                    int finalVal = intcalVal / 12;
                    str = String.valueOf(finalVal) + " years";
                   
                    } else {
                    str = ((JSONObject) value).get(splittednode[splittednode.length - 1]).toString();
                }
                } else {
                LOGGER.info("value = null");
            }
            } else if (value instanceof JSONArray) {
            LOGGER.info("value = ARRAY" + value);
            JSONArray temp = (JSONArray) value;
            for (int j = 0; j < temp.length(); j++) {
                LOGGER.info("splittednode[splittednode.length-1] = " + splittednode[splittednode.length - 1]);
                if (((JSONObject) ((JSONArray) temp).getJSONObject(j)).getString("code").equals(
                splittednode[splittednode.length - 1])) {
                    value = (JSONObject) ((JSONArray) temp).getJSONObject(j);
                    if (((JSONObject) value).getString("value").equals("null")
                    || ((JSONObject) value).getString("value").isEmpty()) {
                        str = ((JSONObject) value).getString("value");
                        } else {
                        str = ((JSONObject) value).getString("value_str");
                    }
                }
            }
        }
        
        GetExcelInput getInput = new GetExcelInput();
        String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(ExcelMethodInput.Openwebpage_MethodNameKey,
        ExcelMethodInput.Openwebpage_ChaneelNameKey);
        if (channelName.equals("auto-loans")) {
            if (str.isEmpty() || str.length() == 0 || str == null) {
                str = "n/a";
            }
        }
        return str;
    }
    private void compareWebAndJson(String sectionName, String valuefromwebsite, String valuefromapi) {
        LOGGER.info("sectionName: " + sectionName);
        LOGGER.info("valuefromwebsite: " + valuefromwebsite);
        LOGGER.info("valuefromapi: " + valuefromapi);
        String finalvaluefromwebsite = removeBraces(valuefromwebsite);
        String finalvaluefromapi = removeBraces(valuefromapi);
        LOGGER.info("valuefromwebsite: " + finalvaluefromwebsite);
        LOGGER.info("valuefromapi: " + valuefromapi);
        LOGGER.info("finalvaluefromapi: " + finalvaluefromapi);
        if (finalvaluefromapi.trim().equals(finalvaluefromwebsite.trim())) {
            LOGGER.info("validation passed");
            if (actualFilterFailureResult.length() > 0) {
                
                } 
            } else {
            LOGGER.info("validation failed");
            isAllDetailsPassed = false;
            if (actualFilterFailureResult.length() > 0) {
                actualFilterFailureResult = actualFilterFailureResult + " , " + "SECTION NAME : " + sectionName
                + " VALIDATION : FAILED for ( Api Value = " + finalvaluefromapi + ", Web Value = "
                + finalvaluefromwebsite + ")";
                } else {
                actualFilterFailureResult = "SECTION NAME : " + sectionName
                + " VALIDATION : FAILED for  ( Api Value = " + finalvaluefromapi + ", Web Value = "
                + finalvaluefromwebsite + ")";
            }
        }
    }
    private String removeBraces(String string) {
        
        if (string.contains("(")) {
            string = string.replace("(", "{").toString().trim();
            }
            if (string.contains(")")) {
            string = string.replace(")", "}").toString().trim();
        }
        if (string.contains("{")) {
                string = string.replace("{", "").trim();
                }
                if (string.contains("reformat_")) {
                    string = string.replaceAll("reformat_", "").toString().trim();
                }
                if (string.contains("raw_")) {
                    string = string.replaceAll("raw_", "").toString().trim();
                }
                if (string.contains("<ul>")) {
                    string = string.replaceAll("<ul>", "").toString().trim();
                }
                if (string.contains("</ul>")) {
                    string = string.replaceAll("</ul>", "").toString().trim();
                }
                if (string.contains("<li>")) {
                    string = string.replaceAll("<li>", "").toString().trim();
                }
                if (string.contains("</li>")) {
                    string = string.replaceAll("</li>", "").toString().trim();
                }
                if (string.contains(".array_value.")) {
                    string = string.replaceAll(".array_value.", ".").toString().trim();
                }
                if (string.contains("_txt")) {
                    string = string.replaceAll("_txt", "").toString().trim();
                }
                if (string.contains("loan_tenure_units")) {
                    string = string.replaceAll("loan_tenure_units", "loan_tenure_unit").toString().trim();
                }
                if (string.contains("is defined ?")) {
                    string = (string.split("is defined ?")[0]).trim();
                }
                if (string.contains("is defined %")) {
                    string = (string.split("is defined %")[0]).trim();
                }
                if (string.contains(".value")) {
                    string = (string.split(".value")[0]).trim().toString();
                    LOGGER.info("string .value removed : " + string);
                }
                if (string.contains("'months'")) {
                    string = (string.split("'months'")[0]).trim().toString();
                    string = string.replaceAll("\\s*$", "").replaceAll("^\\s*", "");
                    LOGGER.info("after string 'months' removed : " + string);
                }
                if (string.contains("apr/12")) {
                    LOGGER.info("INSIDE : " + string);
                    string = (string.split("/12")[0]).trim().toString();
                    LOGGER.info("apr/12 : " + string);
                }
                if (string.contains("% if")) {
                    string = (string.split("% if")[1]).trim().toString();
                    string = (string.split("is empty %")[0]).trim().toString();
                    string = string.replaceAll("\\s*$", "").replaceAll("^\\s*", "");
                    LOGGER.info("is empty : " + string);
                }
                if (string.contains("IDR")) {
                    string = (string.split("IDR")[1]).trim().toString();
                    LOGGER.info("IDR : " + string);
                }
                if (string.contains("months")) {
                    string = (string.split("months")[0]).trim().toString();
                    LOGGER.info("months : " + string);
                }
                if (string.contains("}")) {
            string = string.replace("}", "").trim();
        }
        if (string.contains("|")) {
            string = (string.split("\\|")[0]).trim();
        }
        if (string.contains("%")) {
            string = string.replace("%", "").trim();
        }
        if (string.contains(",")) {
            string = string.replace(",", "").trim();
        }
        if (string.contains("$")) {
            string = string.replace("$", "").trim();
        }
        if (string.contains("Years")) {
            string = string.replace("Years", "").trim();
        }
        return string.trim();
    }
    
    
 
    
    
   
    public WebDriver getWebDriver() throws Exception {
        ExcelInputData excelInput = ExcelInputData.getInstance();
        WebDriver webDriver = excelInput.getWebDriver();
        return webDriver;
    }
    public String Singleproductjson() {
        GetExcelInput getInput = new GetExcelInput();
        String detailPagePermaLink = getInput.get_A_Value_Using_Key_Of_A_Method(
        ExcelMethodInput.DetailPageProduct_MethodNameKey, ExcelMethodInput.DetailPage_PermaLink);
        LOGGER.info("detailPagePermaLink " + detailPagePermaLink);
        String apiurl = MyStore.SINGLEPRODUCTJSONAPI;
        String permalink = "&permalink=" + detailPagePermaLink;
        String permaurl = apiurl + permalink;
        LOGGER.info("perma : " + permaurl);
        WebserviceRequest webserviceRequest = new WebserviceRequest();
        webserviceListener = this;
        LOGGER.info("Webservice Request : " + permaurl);
        String jsonResult = webserviceRequest.GET(webserviceListener, permaurl);
        if (jsonResult.isEmpty() || jsonResult.equals("null")) {
            LOGGER.info("Result is empty or null");
        }
        return jsonResult;
    }
    @Override
    public void webserviceRequestSuccessListener(int statusCode, String statusMessage, String apiResult) {
        
    }
    @Override
    public void webserviceRequestFailureListener(int statusCode, String statusMessage) {
        
    }
}
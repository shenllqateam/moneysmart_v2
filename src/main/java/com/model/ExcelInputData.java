package com.model;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.openqa.selenium.WebDriver;
import com.model.MethodInputData;
public class  ExcelInputData {
    public static  ExcelInputData excelInputObject = null;
    protected static ArrayList<HashMap<String, MethodInputData>> excelInput;
    protected static ArrayList<HashMap<String, ExcelTestimonialData>> excelTestimonialInputData;
    protected static ArrayList<HashMap<String, ExcelIconInputData>> excelIconInputData;
    protected static ArrayList<HashMap<String, ExcelFilterInputData>> excelFilterInputData;
    protected static ArrayList<HashMap<String, ExcelProductValidationInputData>> excelProductValidationInputData;
    public ExcelCalculationInputData excelCalculationInputData;
    public Object yamlInputDataObject;
    public Object yamlConfigDataObject;
    public Object apiJsonObject;
    public WebDriver driver;
    // public ExcelTestimonial excelTestimonialInputData;
    public static ExcelInputData getInstance() {
        if (excelInputObject == null) {
            excelInputObject = new ExcelInputData();
            excelInput = new ArrayList<HashMap<String, MethodInputData>>();
            excelTestimonialInputData = new ArrayList<HashMap<String, ExcelTestimonialData>>();
            excelIconInputData = new ArrayList<HashMap<String, ExcelIconInputData>>();
            /* NLF Filter array */
            excelFilterInputData = new ArrayList<HashMap<String, ExcelFilterInputData>>();
            /* Product Validation array */
            excelProductValidationInputData = new ArrayList<HashMap<String, ExcelProductValidationInputData>>();
        }
        return excelInputObject;
    }
    /*
    * All excel inputs except calculation data, testimonial data and icon
    * data...
    */
    public void setExcelInput(HashMap<String, MethodInputData> methodData) {
        excelInput.add(methodData);
    }
    public ArrayList<HashMap<String, MethodInputData>> getExcelInput() {
        return excelInput;
    }
    /*
    * REad and store YAML Object data only
    */
    public void setYAMLData(Object yamlObject) {
        this.yamlInputDataObject = yamlObject;
    }
    public Object getYAMLData() {
        return yamlInputDataObject;
    }
    /*
    * REad and store YAML Object data only
    */
    public void setYAMLConfigData(Object yamlObject) {
        this.yamlConfigDataObject = yamlObject;
    }
    public Object getYAMLConfigData() {
        return yamlConfigDataObject;
    }
    /*
    * REad and store JSON only
    */
    public void setAPIResultJson(Object apiJsonObject) {
        this.apiJsonObject = apiJsonObject;
    }
    public Object getAPIResultJson() {
        return apiJsonObject;
    }
    /*
    * Calulcation data only
    */
    public void setExcelCalculationInputData(ExcelCalculationInputData excelCalculationInputData) {
        this.excelCalculationInputData = excelCalculationInputData;
    }
    public ExcelCalculationInputData getExcelCalculationInputData() {
        return excelCalculationInputData;
    }
    /*
    * Testimonial data only
    */
    public void setExcelTestimonialInputData(HashMap<String, ExcelTestimonialData> testimonialData) {
        excelTestimonialInputData.add(testimonialData);
    }
    public ArrayList<HashMap<String, ExcelTestimonialData>> getExcelTestimonialInputData() {
        return excelTestimonialInputData;
    }
    /*
    * Icons data only
    */
    public void setExcelIconInputData(HashMap<String, ExcelIconInputData> iconData) {
        excelIconInputData.add(iconData);
    }
    public ArrayList<HashMap<String, ExcelIconInputData>> getExcelIconInputData() {
        return excelIconInputData;
    }
    /*
    * NLF Filter Sections only
    */
    public void setExcelFilterInputData(HashMap<String, ExcelFilterInputData> filterData) {
        excelFilterInputData.add(filterData);
    }
    public ArrayList<HashMap<String, ExcelFilterInputData>> getExcelFilterInputData() {
        return excelFilterInputData;
    }
    /*
    * Product Validation Sections only
    */
    public void setExcelProductValidationInputData(
    HashMap<String, ExcelProductValidationInputData> productValidationData) {
        excelProductValidationInputData.add(productValidationData);
    }
    public ArrayList<HashMap<String, ExcelProductValidationInputData>> getExcelProductValidationInputData() {
        return excelProductValidationInputData;
    }
    /*
    * Set Filter WebDriver to invoke in ProductValidation class
    */
    public void setWebDriver(WebDriver driver) {
        this.driver = driver;
    }
    public WebDriver getWebDriver() {
        return driver;
    }
}
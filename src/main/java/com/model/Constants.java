package com.model;
/**
*
* @author Shenll Technology Solutions
*
*/
public class Constants {
    public static final class ExcelMethodInput {
        /*
        * Initialize openwebpage method key
        */
        public static final String Openwebpage_ChaneelFilterTypeKey = "Category Code";
        public static final String Openwebpage_MethodNameKey = "Open Channel Url";
        public static final String Openwebpage_ChaneelNameKey = "Name";
        public static final String Openwebpage_ChaneelNameKeyinYaml = "Yaml Name";
        public static final String Openwebpage_ChanellURlKey = "URL";
        public static final String Openwebpage_Country = "Country";
        public static final String Less_DetailMethodNameKey = "Less Detail";
        public static final String Apply_Now_Method_NameKey = "Apply Now";
        public static final String Yaml_Chanell_PathKey = " YamlChannelPath";
        public static final String Icon_MethodNameKey = "Icon";
        public static final String FAQ_MethodNameKey = "FAQ";
        public static final String Ask_A_Question_MethodNameKey = "Ask a Question";
        public static final String Refinancing_Rates_MethodNameKey = "Refinancing Rates";
        public static final String Testimonial_MethodNameKey = "Testimonial";
        public static final String CompareValidation_MethodNameKey = "Compare";
        public static final String TitleValidation_MethodNameKey = "Title Validation";
        public static final String DescriptionValidation_MethodNameKey = "Description Validation";
        public static final String BannerValidation_MethodNameKey = "Banner";
        public static final String BankWidgetValidation_MethodNameKey = "Bank widget";
        public static final String ShowMoreResultMethodNameKey = "Show More Result";
        public static final String FacebookLogin_Validation_MethodNameKey = "Facebook Login";
        public static final String EmailLogin_Validation_MethodNameKey = "Email Login";
        public static final String GooglePlusLogin_Validation_MethodNameKey = "GooglePlus Login";
        public static final String Detail_Title_MethodNameKey = "Detail Page Title";
        public static final String Detail_Reasons_MethodNameKey = "Reasons";
        public static final String Expected_Result_Key = "Expected Test Result";
        public static final String Test_Description_Key = "Test Description";
        public static final String Actual_Result_Key = "Actual Result";
        public static final String Detail_ApplyNow_MethodName_Key = "DetailPage Apply NowButton";
        public static final String DetailPage_Footer_ApplyNow_MethodNameKey = "Footer Apply NowButton";
        public static final String TitleValidation_TextKey = "Title";
        public static final String Description_Validation_DescriptionKey = "Description";
        public static final String DetailPageProduct_MethodNameKey = "Detail PageProduct";
        public static final String GotoSite_MethodNameKey = "Go to Site";
        public static final String FooterGotoSite_MethodNameKey = "Footer Go to Site";
        public static final String ProductCount_MethodNameKey = "Product Count Validation";
        public static final String ResultHeadTittle_Validation_MethodNameKey = "Result Head Title";
        public static final String Browser_MethodNameKey = "Browser";
        public static final String Browser_Firefox = "Firefox";
        public static final String Browser_Chrome = "Chrome";
        public static final String Browser_Safari = "Safari";
        public static final String DetailPage_PermaLink = "PermaLink";
        public static final String DetailPage_ProductName = "Detail Product Name";
        /* DETAIL PAGE SECTION STARTS */
        public static final String Detail_ProductKey = "Detail Product";
        public static final String Product_NameKey = "Product Name";
        public static final String Permalink_Key = "Permalink";
        public static final String TravelInconvenience_MethodNameKey = "Travel Inconvenience";
        public static final String MedicalCoverage_MethodNameKey = "Medical Coverage";
        public static final String PersonalProtection_MethodNameKey = "Personal Protection";
        public static final String TravelDelay_webKey = "Travel Delay WebElement";
        public static final String TravelDelay_nodeKey = "Travel Delay";
        public static final String TravelDelay_webTitleKey = "Travel Delay KeyElement";
        public static final String TravelCancelation_webKey = "Travel Cancelation / Postponement WebElement";
        public static final String TravelCancelationnode_Key = "Travel Cancelation / Postponement";
        public static final String TravelCancelation_TitleKey = "Travel Cancelation / Postponement KeyElement";
        public static final String DelayedBaggage_webKey = "Delayed Baggage WebElement";
        public static final String DelayedBaggage_nodeKey = "Delayed Baggage";
        public static final String DelayedBaggage_TitleKey = "Delayed Baggage KeyElement";
        public static final String OverseasMedicalExpenses_webKey = "OverseasMedicalExpenses WebElement";
        public static final String OverseasMedicalExpenses_nodeKey = "Overseas Medical Expenses";
        public static final String OverseasMedicalExpenses_TitleKey = "OverseasMedicalExpenses KeyElement";
        public static final String DailyHospitalAllowance_webKey = "DailyHospitalAllowance WebElement";
        public static final String DailyHospitalAllowance_nodeKey = "Daily Hospital Allowance";
        public static final String DailyHospitalAllowance_TitleKey = "DailyHospitalAllowance KeyElement";
        public static final String DeathDisabillity_webKey = "Death / Total Permanent Disabillity WebElement";
        public static final String DeathDisabillity_nodeKey = "Death / Total Permanent Disabillity";
        public static final String DeathDisabillity_TitleKey = "Death / Total Permanent Disabillity KeyElement";
        public static final String TripCurtailment_webKey = "Trip Curtailment WebElement";
        public static final String TripCurtailment_nodeKey = "Trip Curtailment";
        public static final String TripCurtailment_TitleKey = "Trip Curtailment KeyElement";
        public static final String Missedflightconnection_webKey = "Missed flight connection WebElement";
        public static final String Missedflightconnection_nodeKey = "Missed flight connection";
        public static final String Missedflightconnection_TitleKey = "Missed flight connection KeyElement";
        public static final String LossDamageofBaggage_webKey = "Loss/Damage of Baggage WebElement";
        public static final String LossDamageofBaggage_nodeKey = "Loss/Damage of Baggage";
        public static final String LossDamageofBaggage_TitleKey = "Loss/Damage of Baggage KeyElement";
        public static final String LossofTravelDocuments_webKey = "Loss of Travel Documents WebElement";
        public static final String LossofTravelDocuments_nodeKey = "Loss of Travel Documents";
        public static final String LossofTravelDocuments_TitleKey = "Loss of Travel Documents KeyElement";
        public static final String PostTripMedicalExpenses_webKey = "Post-Trip Medical Expenses WebElement";
        public static final String PostTripMedicalExpenses_nodeKey = "Post-Trip Medical Expenses";
        public static final String PostTripMedicalExpenses_TitleKey = "Post-Trip Medical Expenses KeyElement";
        public static final String SponsoredImageValidation_MethodNameKey = "Sponsored Image";
        /* DETAIL PAGE SECTION ENDS */
    }
    public static final class TestResult {
        /*
        * Initialize test case RESULT KEY
        */
        public static final String R_TESTIMONIAL = "Testimonial";
        public static final String R_ICON = "Icon";
        public static final String R_METHOD_EXECUTE_OR_NOT = "Execute";
        public static final String R_IS_SUCCESS = "IsMethodSuccess";
        public static final String R_IS_EXCEPTION = "ExceptionOccured";
        public static final String R_METHOD_NAME = "MethodName";
        public static final String R_METHOD_PRIOTITY = "MethodPriority";
        public static final String R_MESSAGE = "Message";
        public static final String R_EXCEPTION_ERROR_MESSAGE = "Exception";
        public static final String R_COMMENTS = "Comments";
        public static final String R_ACTUAL_RESULT = "ActualResult";
        public static final String R_EXPECTED_RESULT = "ExpectedResult";
        public static final String R_DESRIPTION = "Description";
        public static final String R_ERROR_RESULT = "ActualResult";
        public static final String R_MULTIPLE_EXECUTE_FOR_TESTIMONIAL = "ExecuteOrNot";
        public static final String R_MULTIPLE_EXECUTE_FOR_ICON = "ExecuteOrNot";
    }
    
    public static class MyStore {
        public static String ACESSTOKEN;
        public static String SINGLEPRODUCTJSONAPI;
           
    }
}
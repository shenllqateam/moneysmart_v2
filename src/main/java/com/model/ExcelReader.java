package com.model;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
public class ExcelReader {
    public static  Sheet wrksheet;
    public static Workbook wrkbook = null;
    protected static final Hashtable<String, Integer> dict = new Hashtable<String, Integer>();
    public ExcelReader(String ExcelSheetPath, String sheetName) throws IOException, BiffException {
        try {
            wrkbook = Workbook.getWorkbook(new File(ExcelSheetPath));
            wrksheet = wrkbook.getSheet(sheetName);
            } catch (IOException e) {
            throw new IOException();
        }
    }
    public static int RowCount() {
        return wrksheet.getRows();
    }
    public static String ReadCell(int column, int row) {
        return wrksheet.getCell(column, row).getContents();
    }
    public void ColumnDictionary(int fromRowCount) {
        // Iterate through all the columns in the Excel sheet and store the
        // value in Hashtable
        for (int col = 0; col < wrksheet.getColumns(); col++) {
            dict.put(ReadCell(col, fromRowCount), col);
        }
    }
    public static int GetCell(String colName) {
        try {
            int value;
            value = ((Integer) dict.get(colName)).intValue();
            return value;
            } catch (NullPointerException e) {
            return (0);
        }
    }
}
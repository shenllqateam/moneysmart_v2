package com.common.utility;
/**
*
* @author Shenll Technology Solutions
*
*/
public interface FTPFileUploadListener {
    public void uploadTaskSuccessListener(int statusCode, String statusMessage);
    public void uploadTaskFailureListener(int statusCode, String statusMessage);
}
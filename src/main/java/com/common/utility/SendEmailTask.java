package com.common.utility;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class SendEmailTask {
    /*
    * Email send status
    */
    private static final Logger LOGGER = Logger.getLogger(SendEmailTask.class.getName());
    private static final int FAILURE_CODE = 0;
    private static final int SUCCESS_CODE = 1;
    private static final String SUCCESS_MESSAGE = "Email sent successfully.";
    private static final String EXCEPTION_MESSAGE = "Exception while sending email : ";
    private String emailSendingStatus = "Email sending status";
    private String exceptionString = "EXCEPTION";
    private String attachmentcontentType = "text/html";
    private String attachmentoutputfolderName = "test-output/";
    public String send(SendEmailTaskListener emailSendlistener, String sendingAttachmentFileName,
    final String authEmail, final String authEmailPassword, String fromuserName, String touserName,
    String fromEmail, String toEmail, String emailSubject, String emailbodyMessage, String ccuserName,
    String ccEmail) {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(new File("settings.properties")));
            } catch (FileNotFoundException e1) {
            LOGGER.log(Level.SEVERE, exceptionString, e1);
            } catch (IOException e1) {
            LOGGER.log(Level.SEVERE, exceptionString, e1);
        }
        Session session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(authEmail, authEmailPassword);
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail));
            message.setSubject(emailSubject);
            BodyPart body = new MimeBodyPart();
            Configuration cfg = new Configuration();
            Template template;
            try {
                template = cfg.getTemplate(attachmentoutputfolderName + sendingAttachmentFileName);
                Map<String, String> rootMap = new HashMap<>();
                rootMap.put("to", touserName);
                rootMap.put("body", emailbodyMessage);
                rootMap.put("from", fromuserName);
                LOGGER.info("CC_USERNAME...." + ccuserName);
                LOGGER.info("CC_EMAIL...." + ccEmail);
                rootMap.put("cc", ccEmail);
                Writer out = new StringWriter();
                try {
                    template.process(rootMap, out);
                    } catch (TemplateException e) {
                    LOGGER.log(Level.SEVERE, exceptionString, e);
                }
                body.setContent(out.toString(), attachmentcontentType);
                } catch (IOException e) {
                LOGGER.log(Level.SEVERE, exceptionString, e);
            }
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(body);
            body = new MimeBodyPart();
            String filename = "hello.txt";
            DataSource source = new FileDataSource(filename);
            body.setDataHandler(new DataHandler(source));
            body.setFileName(filename);
            message.setContent(multipart);
            Transport.send(message);
            LOGGER.info("Sending email....");
            emailSendingStatus = SUCCESS_MESSAGE;
            emailSendlistener.sendEmailSuccessListener(SUCCESS_CODE, emailSendingStatus);
            } catch (MessagingException e) {
            LOGGER.log(Level.SEVERE, exceptionString, e);
            emailSendingStatus = EXCEPTION_MESSAGE + e.getMessage();
            emailSendlistener.sendEmailFailureListener(FAILURE_CODE, emailSendingStatus);
        }
        return emailSendingStatus;
    }
}
package com.common.utility;
/**
*
* @author Shenll Technology Solutions
*
*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
public class WebserviceRequest {
    /*
    * Webservice Request status
    */
    private static final Logger LOGGER = Logger.getLogger(WebserviceRequest.class.getName());
    private static String exceptionString = "EXCEPTION";
    private static final int FAILURE_CODE = 0;
    private static final int SUCCESS_CODE = 1;
    private static final String SUCCESS_MESSAGE = "Webservice retrive result successfully.";
    private static final String EXCEPTION_MESSAGE = "Exception while connecting webservice request : ";
    private String webserviceRequestStatus = "Webservice Request status";
    public String GET(String apiRequestUrl) {
        String getRequestResults = "";
        try {
            getRequestResults = ConntectHttpUrlAndGetResult(apiRequestUrl);
            } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
        }
        return getRequestResults;
    }
    public String GET(WebserviceRequestListener webserviceListener, String apiRequestUrl) {
        String getRequestResults = "";
        try {
            getRequestResults = ConntectHttpUrlAndGetResult(apiRequestUrl);
            webserviceRequestStatus = SUCCESS_MESSAGE;
            webserviceListener.webserviceRequestSuccessListener(SUCCESS_CODE, webserviceRequestStatus,
            getRequestResults);
            } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "EXCEPTION", e);
            webserviceRequestStatus = EXCEPTION_MESSAGE;
            webserviceListener.webserviceRequestFailureListener(FAILURE_CODE, webserviceRequestStatus);
        }
        return getRequestResults;
    }
    public static String ConntectHttpUrlAndGetResult(String apiRequestUrl) throws Exception {
        URL url = null;
        BufferedReader reader = null;
        StringBuilder stringBuilder;
        try {
            url = new URL(apiRequestUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
            } catch (Exception e) {
            LOGGER.log(Level.SEVERE, exceptionString, e);
            throw e;
            } finally {
            if (reader != null) {
                try {
                    reader.close();
                    } catch (IOException ioe) {
                    LOGGER.log(Level.SEVERE, exceptionString, ioe);
                }
            }
        }
    }
}